This document contains some basic notes for using Docker.


### install docker software

In Arch Linux, install the `docker` container, and make sure `docker.service` is running (`# systemctl start docker.service`).


### list, obtain, delete, and run docker images

The table below shows some basic commands, using `centos:8` as the example image name. In `centos:8`, the word `centos` is the repository and `8` is the tag.

| Action          | Command                        |
| ---             | ---                            |
| list all images | `# docker image ls`            |
| obtain an image | `# docker image pull centos:8` |
| delete an image | `# docker image rm centos:8`   |
| run an image    | `# docker run -it centos:8`    |


*ref*: https://wiki.archlinux.org/title/Docker


### create a docker container from an image

1) create a Dockerfile, which will look like this, to install `zsh` for example:
    ```
    FROM archlinux:latest
    USER root
    RUN pacman-key --init
    RUN pacman --noconfirm -Syu zsh
    CMD
    ```
    
2) build an image using the `Dockerfile` with the name `myimage` (the `-f` option can be left out, in which case docker will use a file named `Dockerfile`):  
`# docker build -f <name-of-dockerfile> -t myimage .`

3) run a container based on `myimage`:  
`# docker container run -it --rm --name mycont myimage /bin/bash`


### create two terminals with the same docker container

1) run this command in the first terminal:  
`# docker container run -it --rm --name mycont myimage /bin/bash`

2) run this command in the second terminal:  
`# docker exec -it mycont /bin/bash`


### run a dash/plotly app in a docker container hosted in Windows, and view it in Windows from a browser

1) add this option to the docker container run command, which will map port 9000 in the container to port 9002 on the Windows side:
`-p 9002:9000`

2) use this line in the python file to run the dash app:  
`app.run_server(debug=True, host='0.0.0.0', port=9000)`

3) on a browser in Windows, go to this address to view the plot:
`http://localhost:9002`

*ref*: https://python.plainenglish.io/dockerizing-plotly-dash-5c23009fc10b


### example Dockerfiles

#### Dockerfile to run xeyes

1) run this:  
`$ xhost +local:root`  
*ref:* https://wiki.ros.org/docker/Tutorials/GUI

2) create this Dockerfile:
    ```dockerfile
     FROM archlinux:latest
     RUN pacman-key --init
     RUN pacman --noconfirm -Syu xorg-xeyes
     CMD
    ```

3) Build the Dockerfile and then run the container:  
`# docker container run -it --rm -v /tmp/.X11-unix:/tmp/.X11-unix -e DISPLAY=$DISPLAY --name mycont my_xeyes_image /bin/bash`

4) You may want to run this when you're done:  
`$ xhost -local:root`

#### Dockerfile to run Chromium

1) run this:  
`$ xhost +local:root`  
*ref:* https://wiki.ros.org/docker/Tutorials/GUI

2) create this Dockerfile:
     ```dockerfile
     FROM archlinux:latest
     RUN pacman-key --init
     RUN pacman --noconfirm -Syu chromium
     RUN useradd -m user
     USER user
     CMD
     ```

3) build the Dockerfile then run the container:  
`# docker container run -it --rm --net host -v /tmp.X11-unix:/tmp/.X11-unix -e DISPLAY=$DISPLAY --name mycont my_chromium_image /bin/bash`

4) run:  
`$ chromium --no-sandbox`  
Then navigate to a website such as archlinux.org. Other websites such as Youtube crashed.

5) you may want to run this when you're done:  
`$ xhost -local:root`