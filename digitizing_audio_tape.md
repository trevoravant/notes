# notes on digitizing audio tape (cassette, microcassette, etc.)

## intro

This writeup describes my experience converting audio tape to a digital format using Linux. These are the steps that I followed when using a microcassette player, but would be similar for a different device such as a cassette player.


## microcassettes

To play microcassette tapes, I'm using a handheld microcassette player. The player has three options to play a tape: "fast play", "2.4" and "1.2". The "2.4" and "1.2" refer values refer to standard speeds in centimeters/second which the microcassette tapes are played at. One can easily determine the correct speed just by listening to the sound of the tape being played back at different speeds.

The microcassette player has 3.5mm jack for audio out. I used a "3.5mm to RCA" connector cord to connect the microcassette player to a USB capture card I have which has two RCA connectors for audio. Alternatively, you could use a different cord to connect the microcassette player directly to the microphone port on your computer.


## audio device

Once the microcassette player has been connected to the computer, the audio can be handled using the [ALSA](https://alsa-project.org) project, which is standard in Linux. In ALSA, the audio device from the capture card will be represented by a device specification in the form `hw:X,Y` where `X` is the card number and `Y` is the device number. Devices can be listed using the following command:

``` bash
$ arecord -l
```

On my computer, the output of this command tells me that the USB capture card corresponds to card 2, and only has device 0. So the capture card audio device is `hw:2,0`.


## audio file format

We will record the audio using the WAV format, which is very common for recording uncompressed and raw audio. After recording the audio to a wav file, the audio could then be further converted as need be. For example, the wav file could be converted to mp3 if you want to reduce the file size.


## recording

The shell command below shows how audio from the microcassette player can be recorded.

``` bash
$ arecord -D hw:2,0 -f S16_LE -c 2 -r 48000 audio.wav
```

*explanation of options*:
* `-D hw:2,0`: record from card 2, device 0
* `-f S16_LE`: use the S16_LE (signed 16 bit little endian) sample format. If I didn't use this, or specified another formate, I got the error `Sample format non available`. What determines this format (I think it's the device)?
* `-c 2`: record two channels
* `-r 48000`: use a sampling rate of 48kHz. If I didn't use this, or used a different value, I would get the warning `rate is not accurate`. However, some rates such as `50000Hz` seem to work, so I'm not sure what's going on there...

---

# extra steps

These steps may or may not be necessary. Some of them make use of the Sound eXchange ([SoX](http://sox.sourceforge.net/)) project.

## separating tracks

In some cases, your digitization may be in stereo, but the track may actually only be mono. In this case, you can use the following SoX command to get the separate tracks (you could also use `ffmpeg`):
```bash
$ sox stereo_inputfile.wav left_channel.wav remix 1
$ sox stereo_inputfile.wav right_channel.wav remix 2
```

## removing noise

If your audio contains background noise, you can try to remove it using SoX. The following steps are based off of [this description](https://unix.stackexchange.com/a/427343/294686).

* *Step 1:* Obtain a sample of the background noise (no voices or other sounds) from the original track. There are many tools which can do this, but I prefer a graphical program like `audacity` because you can see the waveforms and more easily locate a section of the track which only has noise. Save the noise sample to a file called `noise_sample.wav`.

* *Step 2:* generate a noise profile  
`$ sox noise_sample.wav -n noiseprof noise_profile_file`

* *Step 3:* remove the noise  
`$ sox audio.wav audio_processed.wav noisered noise_profile_file 0.2`

The `0.2` in the last command is the aggressiveness of the noise reduction. This value should be between 0 to 1, with higher numbers representing a more aggresive approach.

## removing high frequencies

In some of my recordings of voice, there is a high frequency tone in the background which makes it difficult to hear. The command below will remove all freqencies above 3kHz:

```bash
$ sox input.wav output.wav sinc -3k
```
