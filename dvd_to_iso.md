This write-up describes what I have learned about ripping DVDs to ISO files.

**DVD files**

In Linux, an optical disc drive will be located in the `/dev/` directory (e.g. `/dev/sr0`). On my computer (running Arch Linux) the DVD is automatically mounted to a directory such as `/run/media/trevor/NAME_OF_DVD`. This directory will contain the folders `VIDEO_TS` and `AUDIO_TS`, and possibly a `JACKET_P` and/or `DVDccess` folder as well. The `VIDEO_TS` folder contains the DVD's video files in VOB format. These files may not be directly playable because they are encrypted

**unencrypted DVDs**

You can use the following command to make an iso file from a DVD:
```
$ dd if=/path/to/dvd/device of=/path/to/file.iso
```
where `/path/to/dvd/device` will be something like `/dev/sr0`, and `/path/to/file.iso` is where you want to save the iso file. Note that you can use other commands such as `cat`, `tee`, `pv`, `cp`, and `tail` instead of `dd` ([see this post](https://unix.stackexchange.com/a/224314/294686)).

Next, you can use checksums to determine that an iso file is an exact replica of the DVD. First, run `md5sum` on the DVD device:
```
$ md5sum /path/to/dvd/device
```
Then, run `md5sum` on the iso file:
```
$ md5sum /path/to/file.iso
```
The `md5sum` commands should produce the same checksum.

Apparently (although I have not observed this yet), the `dd` command shown above may not work for some DVDs. In these cases, you can use the following, slightly more complicated `dd` command. First, get some information about the DVD device by running the following command:
```
$ isoinfo -d -i /path/to/dvd/device | grep -i -E 'block size|volume size'
```
 Using the logical block size for the `BS=` variable, and volume size for the `COUNT=` variable in the following command, run the following command to create an iso file:
```
$ dd if=/dev/sr0 of=/path/to/file.iso bs=<block_size> count=<volume_size>
```

**encrypted DVDs**

Many DVDs are encrypted with the Content Scramble System (CSS). There are several signs that a DVD is encrypted, includind the VOBs file being unplayable, or playable but with scrambled content. Another sign is receiving an `Input/output error` when trying to copy the VOB files to a different directory. Originally I thought these errors were due to a scratched or otherwise corrupted DVD, but they actually were due to the DVD being encrypted. To work with encrypted DVDs, you have to install `libdvdcss` or a similar package. 

If you attempt to run the `dd` command on an encrypted DVD, you will get an `Input/output error`. To unencrypt the DVD, you can just open the disc in VLC until the DVD menu appears. Then you can close VLC and run the same `dd` commands mentioned above. This should produce an ISO file that can be played in a player like VLC. However, occaisonally the ISO file will still not play properly, and you may have to use the `mkisofs` or `genisoimage` command.


**mkisofs & genisoimage commands**

In addition to using the `dd` command, there are other commands you can use to generate an iso file of a DVD, such as `mkisofs` and `genisoimage`. Note that these two commands are both part of the `cdrtools` library and are actually more or less the same command[*](https://askubuntu.com/questions/557910/difference-between-mkisofs-and-genisoimage).
```
$ mkisofs -dvd-video -udf -o /path/to/file.iso /path/to/mounted/dvd/
$ genisoimage -dvd-video -udf -o /path/to/file.iso /path/to/mounted/dvd/

```
where `/path/to/mounted/dvd/` will be something like `/run/media/trevor/VIDEO/`. I have not needed to use `mkisofs` or `genisoimage` in place of `dd`. Also, a downside of these commands is that the iso file they produce will have a different checksum than that of the DVD device (unlike `dd`).


**references**

* https://unix.stackexchange.com/questions/224277/is-it-better-to-use-cat-dd-pv-or-another-procedure-to-copy-a-cd-dvd
* https://askubuntu.com/a/874945/827176
* https://wiki.archlinux.org/title/dvdbackup
