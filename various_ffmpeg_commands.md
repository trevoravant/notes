**use ffmpeg to capture video from your screen**

First, run the `xrandr` command to determine the screen's current resolution and framerate, which will be marked with an asterisk:
```bash
$ xrandr
...
   3840x2160     60.00*+  30.00
...
```
The output tells us that the screen's resolution is 3840x2160, and its framerate is 60.

Using this information, we can run the following ffmpeg command:
```bash
$ ffmpeg -framerate 60 -probesize 200M -f x11grab -i :0.0+0,0 -preset ultrafast -pix_fmt yuv420p video.mp4
```
*explanation of options:*
- `-framerate 60` sets the record framerate to 60 FPS
- `-probesize 200M` without this, I get the error `Stream #0: not enough frames to estimate rate; consider increasing probesize` (see Stack Overflow answer below)
- `-i :0.0+0,0` sets the origin of the video as the top-left corner
- `-preset ultrafast` is the [fastest setting for the H.264 codec](https://trac.ffmpeg.org/wiki/Encode/H.264)
- `-pix_fmt yuv420p` I seem to get some dropped frames with this, but with `yuv444p` I seem to get dropped frames as well, although it is not shown in ffmpeg's output
- also, you can optionally add `-video_size 3840x2160` but it seems the command seems to work the same without it

Overall, this is the best I can do and it works alright, but I haven't gotten it recording 3840x2160 with 60 FPS in all scenarios without dropped frames.

HOW TO DO THIS USING A GPU???

[reference 1](https://trac.ffmpeg.org/wiki/Capture/Desktop), [reference 2](https://stackoverflow.com/a/57904380/9357589)

---
**use ffmpeg to capture sound from your microphone or speakers**

Run the following command:
```bash
$ ffmpeg -f pulse -ac 2 -i 2 audio.mp3
```
*explanation of options:*
- `-ac 2` IS SUPPOSED TO SET TO RECORD IN STEREO, BUT IT DOESN'T SEEM TO WORK AND I STILL GET THE ERROR: `Guessed Channel Layout for Input Stream #0.0 : stereo`
- `-i 2` sets the hardware device, change this for microphone or speakers, I CANNOT USE hw:X,Y AND I DON'T KNOW HOW TO DETERMINE THE SETTINGS

---
**use ffmpeg to capture both video from your screen and sound from your microphone/speakers**

Combine the two sections above.

---
**overlay timestamps on a video**

To place a timestamp of the form *HH:MM:SS.MIL*, add the following video filter to your command:
```
-vf "drawtext=fontsize=60:fontcolor=yellow:text='%{pts\:hms}':x=(w-text_w):y=(h-text_h)"
```

[reference](https://www.reddit.com/r/ffmpeg/comments/m6hva1/beautify_timestamp_to_mmsssss/)