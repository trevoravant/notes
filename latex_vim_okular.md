# using Vim & Okular to work with Latex documents

This writup describes how to use Vim to edit a Latex file, Okular to view the corresponding pdf, and enable forward and backward search between the two.


**1. compile the tex file with SyncTeX support**
---

First, make sure you compile your Latex document with SyncTeX. This will generate a `.synctex` file (in addition to the `.pdf` file) which provides the mapping between the tex file(s) and pdf file. You can compile a `.tex` file with Synctex support using either the `pdflatex` or `latexmk` shell commands:

``` bash
$ pdflatex -synctex=-1 file.tex
```
or

``` bash
$ latexmk -pdf -pdflatex="pdflatex -synctex=-1" -cd /path/to/file.tex
```
Note that you can also generate a zipped `.synctex.gz` file by replacing the `-1` in the commands above with a `1`. This may be helpful to reduce file size. As an alternative to using the `-synctex` flag, you can tell the compiler to use Synctex by putting the line `\synctex=1` in the preamble of your Latex file. As far as I know, you can't generate an uncompressed `.synctex` file this way though.

You can run the `pdflatex` and `latexmk` compilation commands from a terminal. Alternatively, you can run these commands from Vim itself. A quick way to do so would be to enter the following command in Vim's colon command line `:!latexmk -pdf -pdflatex="pdflatex -synctex=-1" -cd %` (note `%` represents the path to the current file). But this method may not be ideal if you often edit and compile tex files. Other ways to compile a tex file from Vim are described in the answers to [this question][compile].


**2. tex-to-pdf search (Vim to Okular)**
---

If you compiled the tex file as in step 1, you should have a Synctex (`.synctex` or `.synctex.gz`) file along with the corresponding tex and pdf files. You can then issue the following shell command to use Okular to open the pdf to the location which corresponds to *line_number* in the tex file:

``` bash
$ okular --noraise --unique "/path/to/file.pdf#src:line_number /path/to/file.tex"
```
Note the quotation marks are necessary. Also note that the `--unique` option will ensure only one Okular window is opened, and the `--noraise` option will prevent the Okular window from being focused and raised. This will be helpful in the next step so that Vim isn't exited every time a search is done<sup>1</sup>.

The next step is to run the `okular` shell command from Vim, using the line number the cursor is on. As an example of how to do this, you can add the following to your `.vimrc` file:


``` vim
function! OkularFind()
    let this_tex_file = expand('%:p')
    let master_tex_file = this_tex_file
    let pdf_file = fnamemodify(master_tex_file, ':p:r') . '.pdf'
    let line_number = line('.')
    let okular_cmd = 'okular --noraise --unique "' . pdf_file . '#src:' . line_number . ' ' . this_tex_file . '"'
    let s:okular_job = job_start(['/bin/bash', '-c', okular_cmd])
endfunction
nnoremap <leader>f :call OkularFind()<cr>
```
The last line just defines a keymap to apply the function (note: the `<leader>` key is <kbd>\\</kbd> by default).

Also, this code assumes that your project only involves one tex file. If your project involves a master tex file and other tex files referenced by the master, you will have to change the `master_tex_file` variable.



**3. pdf-to-tex search (Okular to Vim)**
---

The way I know how to do Okular-to-Vim search is by using a server in Vim. Note that I figured out some of the following information from the documentation and issues of [vimtex][vt].

To use a server in Vim, your Vim installation must be compiled with the `+clientserver` option<sup>2</sup>. Note that you can check to see if this option is installed by using Vim's `:version` command. To start a server named "VIM" in Vim, you can run Vim as

``` bash
$ vim --servername VIM
```
If Vim is already running, you can start a server from within Vim with

``` vim
:call remote_startserver('VIM')
```

With Vim running as a server, you can jump to *line_number* of a tex file with the shell command

``` bash
$ vim --remote +line_number /path/to/file.tex
```

The next step is to run that `vim --remote` command from Okular. When viewing a pdf that is part of a tex project, Okular's *Editor* feature has the native ability to map locations in the pdf to lines in the corresponding tex file, by means of the Synctex file. Upon a <kbd>Shift</kbd> + <kbd>Left Mouse Button</kbd> (when using the *Browse* tool) the *Editor* will run a shell command, which is intended to open a text editor to view the tex file. As part of this shell command, Okular provides the symbols `%l` and `%f` to be used as placeholders for the line number and filename (see [The Okular Handbook][ok] for more info). So, search from Okular to Vim can be accomplished with the following steps:

* go to *Settings* → *Configure Okular* → *Editor* and select *Custom Text Editor*, and in *Command* type<sup>3</sup>

        vim --remote +%l %f

* make sure you're using the *Browse Tool* (*Tools* → *Browse*)

* put the cursor over the part of the pdf you want to search for, and do <kbd>Shift</kbd> + <kbd>Left Mouse Button</kbd>

The last step should take you to the corresponding line in your tex file<sup>4</sup>.

<hr>

**notes:**
---

<sup>1</sup> The `--noraise` option will prevent the Okular window from being focused and raised. You can leave this option off if you want to focus the Okular window. Additionally, if you want to raise, but not focus the Okular window, you can combine the `okular` command with a shell command that can interact with windows, for example `xdotool` or `wmctrl`. As an example, you could augment the `okular` shell command above like this:

``` bash
this_WID=$(xdotool getactivewindow) && \
okular --unique "/path/to/file.pdf#src:line_number /path/to/file.tex" && \ 
xdotool windowactivate $this_WID
```

This will focus the Okular window the first time it is opened, but not subsequent times.


<sup>2</sup> In Arch Linux, the default `vim` package is not compiled with the `+clientserver` option, so you must install the `gvim` package, which will install gVim, as well as a more fully-featured version of Vim which is compiled with the `+clientserver` option.


<sup>3</sup> You should also be able to leave off the `%f` because Okular should add it to the end of the command automatically.


<sup>4</sup> Depending on your system configuration, this may or may not focus the Vim window. See the answers to [this question][tex1] and [this question][tex2] for workarounds.


  [ac]: https://tex.stackexchange.com/a/2947/155760
  [compile]: https://vi.stackexchange.com/questions/7126/how-do-i-get-vim-to-compile-my-tex-file-when-executing-w
  [vt]: https://github.com/lervag/vimtex
  [ok]: https://docs.kde.org/stable5/en/kdegraphics/okular/inverse_search.html
  [tex1]: https://vi.stackexchange.com/questions/7902/how-do-i-prevent-a-gvim-server-window-stealing-focus-when-receiving-a-remote-com
  [tex2]: https://tex.stackexchange.com/questions/529518/how-to-focus-vim-when-doing-pdf-to-tex-search-from-okular
