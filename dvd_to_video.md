This document describes my experience ripping video files from a DVD.


## making an ISO file from the DVD

The first step in ripping a DVD is creating an ISO file, which is a clone of the DVD. Instructions on how to convert DVDs to ISO files is explained in **[this document](dvd_to_iso.md)**.


## obtaining VOB video files from the ISO file

**method 1: mplayer (recommended)**

You can use [mplayer](http://www.mplayerhq.hu/) to obtain each video feature of a DVD separately. Before using mplayer, you may first want to identify the titles of the DVD using [lsdvd](https://sourceforge.net/projects/lsdvd/):
```bash
lsdvd /dev/sr0
```

Then, you can use the following command to extract title 4 of the DVD, for example, to a VOB file:
```bash
mplayer dvd://4 -dvd-device /dev/sr0 -dumpstream -dumpfile /path/to/save/file.vob
```

**method 2: manual method (works only for unencrypted DVDs)**

If the DVD is not encrypted, you can directly obtain the VOB files from the `VIDEO_TS` directory. Videos on the DVDs are often split up across multiple VOB files. This is done to keep the file size less than 1 GB in order to maintain compatibility with all operating systems[*](https://en.wikipedia.org/wiki/VOB). In many DVDs, the main feature is often split across multiple files, with the first part of the video being contained in a 1 GB file (e.g. `VTS_02_1.VOB`) and the remainder being contained in addtional files (e.g. `VTS_02_2.VOB`, `VTS_02_3.VOB`, etc.). To join VOB files together, you can use the `cat` command: `cat VTS_07_01.VOB VTS_07_2.VOB VTS_07_03.VOB > combined_file.vob`.

Note that this method may work just fine for some DVDs. However, other DVDs may have an unintuitive correspondance between the videos and VOB files. For example, some DVDs have several separate bonus videos, which are all stored one-after-another in a long VOB file. In this case, you could cut the individual videos from the long VOB file, but that might be a bit tedious, which is why it is recommended to use method 1.


**method 3: dvdbackup (manual method for encrypted DVDs)**

If the DVD is encrypted, you can use the `dvdbackup` package to clone the DVD's original file structure into an unencrypted form: `dvdbackup -i /dev/sr0 -o /path/to/save -M`. You can then use the steps from method 2, but you may run into the same challenges, which is why I recommend using method 1.

## scan type: interlaced v. progressive

VOB files from DVDs will have one of two scan types: interlaced or progressive. In progressive videos, every frame is followed by a brand new frame. In interlaced videos, the frames are designed to be meshed in such a way that certain horizontal rows one frame will mesh the next frame of a video. Interlaced videos are more complicated to deal with then progressive videos. You can determine if a VOB file is interlaced using the `mediainfo` or `ffprobe` commands. Interlacing will often give the appearance of horizontal lines across some frames of a video (known as "combing"). This effect may be subtle, but may be more obvious if you freeze frame fast moving objects in the video. You can remove the interlacing effect using a deinterlacing filter. The most accurate results will come setting the filter to output one frame per field, which effectively doubles the frame rate. So, for a 29.97 FPS VOB file from a DVD, the deinterlacer will produce a video that has 59.94 FPS. Note that any shots in the DVD for which the original camera footage was at 29.97 FPS or lower, doubling the frame rate will not noticably improve the converted video. However, many DVDs were produced with a mix of 30 FPS footage and 60 FPS footage, so to best preserve the original video, it is a good idea to double the frame rate.

A common deinterlacing filter is `ffmpeg`'s `yadif` filter. However, better results can be obtained by using [QTGMC](http://avisynth.nl/index.php/QTGMC). QTGMC can be run in VapourSynth, which involves creating a script in the form of a .vpy file, and using the `vspipe` shell command along with `ffmpeg`. Examples are shown later in this document.

## aspect ratios and non-square pixels

Some video files are stored at a different aspect ratio than they are intendend to be viewed at. In other words, they are meant to be viewed using rectangular (non-square) pixels. For example, a video may be stored as a grid of 100x100 pixels, but may be intended to be played at a 2:1 aspect ratio. As a result, the video must be viewed on pixels that are twice as wide as they are tall. The video files on a DVD are usually stored with non-squre pixels, which makes dealing with them more complicated.

A video with non-square pixels will have three different aspect ratios which must be considered: storage aspect ratio (SAR), display aspect ratio (DAR), and pixel aspect ratio (PAR). The storage aspect ratio is the aspect ratio of the grid of pixels that is stored in the video file, the display aspect ratio is the aspect ratio the video is intended to be played at, and the pixel aspect ratio (PAR) is the aspect ratio of the pixels themselves. The three ratios are related by the equation "SAR×PAR=DAR".

The VOB files on most DVDs I have come across are stored at 720x480 resolution (SAR=3:2), but are meant to be played at a DAR of 4:3. This information can be determined by the command `ffprobe file.VOB`. However, this is not the case for all VOB files, so it important to check each one before conversion. To make things simple and avoid dealing with non-square pixels in the future, we will convert the VOB files to have square pixels (PAR=1).

## audio streams

VOB files on a DVD will have one or more audio streams. Most will have a single stream, which is usually in the form "stereo" but may also be "2 channel" or "5.1" (surround sound with 6 channels). Additionally, some VOB files have multiple audio streams. For example, a VOB file may have one stream for the main video audio and a second stream for a director's commmentary. Another VOB may have one stream for the main audio in "5.1" form, and a second stream for the main audio in "stereo" form. By default, `ffmpeg` will use the first audio stream when converting a VOB, so you if you want to use a different stream, you will have to explicitly tell `ffmpeg` to do so. You can use the `-map` option to do so, for example if a VOB file has one video stream and two audio streams, to use the second audio stream you can do something like `ffmpeg -i video.vob -map 0:0 -map 0:2 ...`.

Each audio stream will have a specific encoding. Most VOB files I come across are encoded using the AC3 codec at a 48kHz sample rate and 192kb/s bit rate. If you do not explicitly set ffmpeg's conversion settings, it often seems to use the AAC-LC (Advanced Audio Codec - Low Compression) codec[*](https://trac.ffmpeg.org/wiki/Encode/AAC) at a 48kHz sample rate and 128kb/s bit rate. Note that these particular settings can be explicitly set using `-c:a aac -b:a 128k -ar 48k` in the ffmpeg command.

Most audio streams on DVDs are stereo, but some are 5.1 surround sound. On some VOB files, the stream will be specified as `5.1(side)`. However, I don't think it is too drastic to consider these to be `5.1` (see [this](https://dsp.stackexchange.com/questions/23611/5-1-rear-to-5-1-side-mixing-matrix) and the "Youtube uploads" section below for additional information).

## simple example

We consider an interlaced, bottom-field-first VOB file from a DVD that is stored at 720x480 resolution with SAR=3:2 and DAR=4:3. Using square pixels, the minimum resolution of this conversion without losing quality is 720x540. First, we create the following QTGMC script:
```bash
$ cat file.vpy
import vapoursynth as vs
import havsfunc as haf

clip = vs.core.ffms2.Source(source='file.vob')
clip = vs.core.resize.Point(clip, format=vs.YUV420P8)
clip = haf.QTGMC(clip, Preset='Very Slow', TFF=False)
clip = vs.core.resize.Lanczos(clip=clip, width=720, height=480, format=vs.YUV420P8)

clip.set_output()
```
Then we run the following shell command:
``` bash
vspipe -c y4m file.vpy - | ffmpeg -i - -i file.vob -map 0:v -map 1:a -c:v libx264 -crf 17 -c:a aac -b:a 128k -ar 48k file.mp4
```
explanation of options:
* `-c:v libx264`: `-c:v` is short for `-codec:v` which tells ffmpeg to set the video encoder. `libx264` is the [x264](https://www.videolan.org/developers/x264.html) encoder which encodes video to the H.264 video compression standard.
* `-crf 17`: CRF means [constant rate factor](https://trac.ffmpeg.org/wiki/Encode/H.264#crf) and lower values correspond to better quality but larger file size
* `-c:a aac`: use the AAC-LC codec to encode the audio
* `-b:a 128k`: use a 128kb/s bit rate for the audio
* `-ar 48k`: use a 48kHz sample rate for the audio

## additional note: Youtube uploads

If you want to upload this video to Youtube, note that Youtube only supports square pixels (PAR=1). If your video does not have square pixels, then Youtube will convert it to have square pixels after you upload it, so it makes sense to convert your video to square pixels before you upload it. Furthermore, Youtube only allows supports certain video resolutions: 360p, 480p, 720p, etc. The term "480p", for example, describes all resolutions with 480 vertical pixels and progresseive scan (i.e. non-interlaced). Some examples of 480p resolutions are 600x480p, 640x480p, and 800x480p. If you upload a video that is not at one of these resolutions, then Youtube will scale *down* your uploaded video to the nearest supported resolution. For example, for a video with DAR=4:3, Youtube supports the resolutions shown in the first row of the table below. Considering these resolutions, since the original VOB file from the DVD is stored at 720x480, you want to convert your video to at least the 960x720 (720p) resolution to prevent Youtube from downscaling your video.

Furthermore, Youtube will limit the bit rate of uploaded video, and supports higher bit rates for higher resolution videos. For example, if you upload a video at 7Mbps, Youtube may convert it to 1Mbps. So, you can achieve better quality on Youtube by uploading it at a higher resolution. For example, if your original video file is 720x480 with DAR=4:3, then it actually may want to convert and upload it at 2880x2160 (2160p) rather than 960x720 (720p) (and therefore use `scale=2880x2160` in the ffmpeg command). I have found that to obtain a visually lossless Youtube video, you should upload the video to Youtube at at least 1440p. However, I usually upload videos at 2160p ("4K") to provide an extra buffer.

| DAR  | 144p    | 240p     | 360p    | 480p     | 720p    | 1080p     | 1440p     | 2160p |
| :--  |:--      |:--       |:--      |:--       | :--     | :--       | :--       | :--   |
| 4:3  | 192x144 | 320x240  | 480x360 |  640x480 | 960x720 | 1440x1080 | 1920x1440 | 2880x2160 |
| 16:9 | 256x144 | ≈427x240 | 640x360 | ≈854x480 | 1280x720| 1920x1080 | 2560x1440 | 3840x2160 |

In its processing, Youtube will also convert the audio of an uploaded video. So you want to upload your video to Youtube with the highest quality audio possible so that the resulting audio of the Youtube video will be as close as possible to the original audio. To do this, you can specify an extremely high bit rate (e.g., 1 gigabit) in the `ffmpeg` command to convert the audio. `ffmpeg` won't be able to achieve such a high bit rate, but will use the maximum possible allowable value.

The audio stream in the VOB file will have a specific channel layout. The channel layouts I have come across for VOB files are "stereo", "2 channel", and "5.1(side)". Audio streams at 5.1 have 6 channels: front left, front right, center, low-frequency effects, surround left, and surround right. The last two channels are sometimes called "back left" and "back right". Audio streams at 5.1(side) have 6 channels which are nearly identical to 5.1: front left, front right, center, low-frequency effects, side left, and side right. So I don't think it is too drastic to consider the "side left" and "side right" channels to be "surround left" and "surround right". If you encounter a 5.1(side) channel, you can convert it to 5.1 (which I believe Youtube prefers) using the ffmpeg option `-channel_layout "5.1"`. 

There are only two types of VOB files I've come across: interlaced scan with DAR=4:3, and progressive scan with DAR=16:9. Here is how to convert both of them so they can be uploaded as a 4K video on Youtube. Note that these commands contain several options such as `-pix_fmt`, `-bf`, `-g` which help convert the video to [Youtube's recommended upload encoding settings](https://support.google.com/youtube/answer/1722171?hl=en).

**VOB file with interlaced scan and DAR=4:3**  
Create this vpy script:
```bash
$ cat file.vpy
import vapoursynth as vs
import havsfunc as haf

clip = vs.core.ffms2.Source(source='file.vob')
clip = vs.core.resize.Point(clip, format=vs.YUV420P8)
clip = haf.QTGMC(clip, Preset='Very Slow', TFF=False)
clip = vs.core.resize.Lanczos(clip=clip, width=2880, height=2160, format=vs.YUV420P8)

clip.set_output()
```
Then run this shell command:
``` bash
vspipe -c y4m file.vpy - | ffmpeg -i - -i file.vob -map 0:v -map 1:a -c:v libx264 -x264-params open_gop=0 -profile:v high -crf 15 -pix_fmt yuv420p -bf 2 -g 30 -coder ac -color_primaries 1 -color_trc 1 -colorspace 1 -movflags faststart -c:a aac -b:a 1G file.mp4
```
**VOB file with progressive scan and DAR=16:9**
``` bash
ffmpeg -i video.vob -vf "scale=3840x2160:flags=lanczos,setsar=1" -c:v libx264 -x264-params open_gop=0 -profile:v high -crf 15 -pix_fmt yuv420p -bf 2 -g 15 -coder ac -color_primaries 1 -color_trc 1 -colorspace 1 -movflags faststart -c:a aac -b:a 1G video.mp4
```
Note that in `ffmpeg`, "sar" denotes "sample aspect ratio" and is equivalent to what we have called "PAR".

## additional note: GPU-acceleration

The ffmpeg commands in this write-up run on the CPU. However, it is possible to run the ffmpeg command on a GPU, which could potentially be more efficient. For example, with an Nvidia GPU, the `h264_nvenc` encoder can be used in place of `libx264`. However, `h264_nvenc` has different options than `libx264`, so using it requires more investigation.

## references

* http://www.jpdh.ga/tips/rip.html

* https://www.benhup.com/technology/dvd-original-one-file-rip-deinterlace-and-add-chapters-with-ffmpeg/

* https://unix.stackexchange.com/questions/43013/rip-chapters-of-a-dvd-to-separate-files

<!---
NOTES TO SELF:


1) CUTTING A PIECE FROM AN MP4

ffmpeg -i video.mp4 -ss 00:01:29.320 -to 00:04:35.680 -c copy -movflags faststart video_cut.mp4


2) PLAY VIDEO WITH MPLAYER, BUT DON'T CLOSE THE WINDOW AT THE END

mplayer -idle -fixed-vo video.mp4


3) PLAY VIDEO WITH MPLAYER AND SHOW TIMESTAMP

mplayer -osdlevel 3 -osd-fractions 1 video.mp4

or press the "o" key


4) COMMON DVD FILE FORMATS, PAR=1 RESOLUTIONS, AND CLARITIES:

720x480 DAR=4:3 clarities:
    1080p is clearer than 720p, 1440p very slightly clearer than 1080p

720x480 DAR=16:9 clarities:
    720p is clearer than 480p, 1080p is slightly clearer than 720p, 1440p is very slightly clearer than 1080p


5) MAKING THUMBNAILS FOR YOUTUBE (RECOMMENDED SIZE: 1280x720)

for an image of size 2880x2160 (DAR=4:3):

ffmpeg to 1280x960 (for 4:3) or 1280x720 (16:9)
ffmpeg -i file.mp4 %06d.png
convert image.png -gravity center -crop 1280x720+0+0 image_thumbnail.png

Videos with DAR=4:3 will sometimes be letterboxed. So to get a clean thumbnail, you often want to scale the video to a 4:3 resolution larger than 1280x960, and then crop out the center 1280x960. Note that when doing this with the `convert` command, you might have to use the `+repage` flag. Below are 4:3 resolutions larger than 1280x960 which ffmpeg can scale to:

1280x960, 1288x966, 1296x972, 1304x978, 1312x984, 1320x990, 1328x996, 1336x1002, 1344x1008, 1352x1014, 1360x1020, 1368x1026, 1376x1032, 1384x1038, 1392x1044, 1400x1050, 1408x1056 1416x1062, 1424x1068, 1432x1074, 1440x1080, 1448x1086, 1456x1092, 1464x1098, 1472x1104, 1480x1110, 1488x1116, 1496x1122, 1504x1128, 1512x1134, 1520x1140

for 16:9 resolutions larger than 1280x720, ffmpeg can scale to:

1280x720, 1312x738, 1344x756, 1376x774, 1408x792, 1440x810, 1472x828


6) DETERMINING IF STREAMS ARE THE SAME

Use the following command, which will give an MD5 hash of the stream. Then run the same command on the other file, and see if the hashes are the same.

ffmpeg -loglevel error -i file.mp4 -map 0:v -f md5 -

see: https://stackoverflow.com/questions/25774996/how-to-compare-show-the-difference-between-2-videos-in-ffmpeg


7) MAKING THE MP4 HALF SPEED

- add a "setpts=2.0*PTs" video filter
- add a "atempo=0.5" audio filter
- change the frame rate to the desired value
- set the "-g" option appropriately (for Youtube)

Here is an example for interlaced scan and DAR=4:3

ffmpeg -i video.vob -r 29.97 -vf "yadif=1,scale=2880x2160:flags=lanczos,setsar=1,setpts=2.0*PTS" -c:v libx264 -crf 15 -pix_fmt yuv420p -bf 2 -g 15 -color_primaries 1 -color_trc 1 -colorspace 1 -movflags faststart -c:a aac -b:a 1G -af "atempo=0.5" video.mp4


-->
