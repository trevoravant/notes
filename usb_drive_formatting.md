This document contains some instructions and tips for formatting a USB drive in Linux. I always seem to encounter problems doing this.


**1) determine the block device name of the drive**

```
# fdisk -l
```

or

```
$ lsblk
```

**2) create a partition**

```
# umount /location/of/partition
# fdisk /location/of/drive
```

where `/location/of/partition` will be something like `/dev/sdb1`, and `/location/of/drive` will be something like `/dev/sdb`. In fdisk, your sequence of commands may be something like `g` (make new GPT partition table), `n` (add new partition), `Enter` `Enter` `Enter` (accept the defaults), and then `w` (write to disk and exit).


**3) format the drive**

To format the drive as VFAT do:

```
# mkfs.vfat -n 'NAME_OF_THE_PARTITION' /location/of/partition
```

where `NAME_OF_THE_PARTITION` is a name you choose.

To format the drive as ext4 do:

```
# mkfs.ext4 /location/of/partition -L 'NAME_OF_THE_PARTITION'
```

Often when I do this, I do not have write permissions on the drive when it is mounted. The permissions can be determined by running `ls -l /run/media/trevor/` which tells me that the owner and group are both "root". I can fix this by (is there a better way?):

```
# chown trevor:trevor /location/partition/is/mounted
```

where `/location/partition/is/mounted` will be something like `/run/media/trevor/3d89-5bfb`.
