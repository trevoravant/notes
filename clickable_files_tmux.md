This document describes how to configure tmux so that when a line containing a filepath and line number is double-clicked, vim will open that filepath at the line number.

### summary

Shell commands often output a filename and corresponding line number, which may represent a search match, or the location of a compilation error.
The goal of this write-up is to describe how to open these filepath-linumber pairs in tmux by double-clicking on the line.

Summary of Steps:
1) Create a command in your `~/.tmux.conf` which will send a double-clicked line to the shell script `open_filepath.sh`
2) Create a shell script named `open_filepath.sh`
    * Use `sed` to determine if the line is of a certain format
    * Use `sed` to extract the filepath and line number form that line
    * Open the filepath at the line number with `vim`


### obtaining lines in tmux with a double-click

Commands in `tmux` can be set in the tmux config file (e.g., `~/.tmux.conf`), or run from the command line.
For example, in tmux, you can enter copy mode by typing <kbd>Ctrl</kbd>+<kbd>b</kbd>, then pressing <kbd>:</kbd> to open the command prompt, and then entering the command `display-message '#{pane_current_path}'`.
Alternatively, you can run the same command from the shell as:
```shell
$ tmux display-message '#{pane_current_path}'
```

In `tmux`, we can obtain a line by double-clicking on it, and send it to a shell script named `open_filepath.sh` as follows:
```tmux
set -g mouse on
bind-key -n DoubleClick1Pane select-pane \; copy-mode \; send-keys -X copy-pipe-line-and-cancel "xargs -d '\n' open_filepath.sh"
```
Note that the `\;` strings represent separation between tmux commands.
Also note that the `-d '\n'` in the `xargs` command ensures that any trailing whitespace is not deleted.


### filepath-linenumber pairs

Different commands output a filenames and corresponding line numbers in different formats.
The table below shows some examples:
| command  | output |
| ---      | ---    |
| `cmake`  | `/home/trevor/file.txt:13:3 error: blah blah blah` |
| `grep`   | `file.txt:13:blah blah blah` |
| `python` | `  File "/home/trevor/file.txt", line 13, in blah blah blah` |

Each output has a different format and therefore requires a unique method to extract the filepath and line number.
Furthermore, as in the `grep` example, sometimes the filepaths will be relative, which necessitates obtaining the directory that `grep` was run in to determine the absolute filepath.


### extracting the filepath and line number from the lines

#### example 1: `absolutepath:linenum: blah blah blah`

We now consider the example in which the output is in this form (e.g., the output of `cmake`):
```
absolutepath:linenum: blah blah blah
```
We will use the `sed` command to process the output lines and extract the filepath and line numbers.
For the `cmake` output, we can create the following `sed` command to extract the filepath and line number:
``` shell
$ sed -n 's/^\(\/[a-zA-Z0-9_\/\.\-]\+\):\([0-9]\+\):.*$/\1 \2/p'
```
where
- `-n` denotes "suppress automatic printing of pattern space"
- `s` denotes substitution
- `/` denotes a separator
- `^` denotes the start of a line
- `\(` denotes the beginning of a capture
- `\/` denotes the `/` character
- `[a-zA-Z0-9_\/\.\-]` denotes any one of these characters: `a` to `z`, `A` to `Z`, `0` to `9`, `_`, `/`, `.`, `-`
- `\+` denotes one or more of the characters above
- `\)` denotes the end of the capture
- `:` denotes the `:` character
- `.*` denotes any combination of characters (`.` denotes any character, and `*` denotes "zero or more times")
- `$` denotes the end of a line
- `\1` denotes the first capture
- ` ` denotes a space character
- `\2` denotes the second capture
- `p` denotes "print the current pattern space"

The `-n` flag and `p` flag together will make it so sed only prints something when a match occurs.
So, the command will operate on the `cmake` example as follows:
```shell
$ sed -n 's/^\(\/[a-zA-Z0-9_\/\.\-]\+\):\([0-9]\+\):.*$/\1 \2/p' <<< '/home/trevor/file.txt:20:3 error: blah blah blah'
/home/trevor/file.txt 20
```
For any string that does not match the pattern in the sed command, sed will not return anything.
Note that `<<<` is called a "here string" and feeds the RHS into stdin of the LHS.


#### example 2: `relativepath:linenum:blah blah blah`

We now move on to slightly more difficult example.
We consider the case when the filepath is relative instead of absolute.
More specifically, we consider the case in which the output is in this form (e.g., the output of `grep`):
```
relativepath:linenum:blah blah blah
```

We can use an almost identical `sed` command to extract the filepath and line number:
```shell
$ sed -n 's/^\([^\/][a-zA-Z0-9_\/\.\-]\+\):\([0-9]\+\):.*$/\1 \2/p'
```
The biggest difference from the previous command is that `\/` has been replaced with `[^\/]`.
This means we're no longer looking for the `/` character at the beginning of a line, but rather any character that is not `/`.

We can determine the absolute filepath by finding the directory of the command which was run to generate that output, and append the relative filepath to that directory.
We find the directory by determining the directory of the `tmux` pane with this `tmux` command:
```shell
$ tmux display-message -p -t output '#{pane_current_path}'
```


#### note: obtaining lines above the main line

In tmux, this command will find a line begining with `line 1`, and then find the line above it beginning with `line 2`:
```shell
$ tmux copy-mode \; send-keys -X search-backward "^line 1" \; send-keys -X search-backward "^line 2" \; send-keys -X copy-pipe-line-and-cancel \; display-message -p "#{buffer_sample}"
```
Note that this command will output a string with `\n` at the end.


#### note: obtaining a top-level git directory

The output of git commands will show a relative path which is with respect to the top-level git directory.
Therefore, we will have to determine the top-level directory to get the absolute path of the file.
To do so, we can use
```shell
$ tmux display-message -p -t output '#{pane_current_path}'
$ git -C /path/to/directory rev-parse --show-toplevel
```
where `/path/to/directory` can be determinined using the `tmux display-message` command shown earlier.


#### note: removing any non-ASCII characters

Occaisonally, the line copied via tmux will contain non-ASCII characters which can cause issues for other commands.
To remove non-ASCII characters, we can use [the following command](https://unix.stackexchange.com/questions/403015/remove-all-type-of-special-characters-in-unix-csv-file/403021#403021):
```shell
$ tr -cd '\000-\177' <<< $line
```
where `line` is the variable containing the line from tmux.


### vim command to open the filepath at the line number

You can open a `<filepath>` to a `<linenumber>` in `vim` with the following command:
```shell
$ vim +<linenumber> <filepath>
```
Or, to open it in an already running server, the command is:
```shell
$ vim --remote +<linenumber> <filepath>
```


### example shell script

An example `open_filepath.sh` shell script to process `absolutepath:linenum:columnnum blah blah blah` strings is shown below. Other formats can be added to that script as needed.
```sh
#!/bin/zsh
# absolutepath:linenum:columnnum blah blah blah
filepath_and_linenum=$(sed -n 's/^\(\/[a-zA-Z0-9_\/\.\-]\+\):\([0-9]\+\):[0-9]\+.*$/\1 \2/p' <<< $1)
if [[ -n $filepath_and_linenum ]]; then
	filepath=$(echo $filepath_and_linenum | cut -d ' ' -f 1)
	linenum=$(echo $filepath_and_linenum | cut -d ' ' -f 2)
	vim_cmd='vim --remote +'$linenum' '$filepath
	eval ${vim_cmd}
fi
```
