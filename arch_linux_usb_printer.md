These are some notes about how to connect a printer over USB in Arch Linux. All of this information is thoroughly documented on the [Arch Wiki CUPS page](https://wiki.archlinux.org/index.php/CUPS), but I always get tripped up by this process, so these are notes to help me.

### 1. install CUPS

First, install the CUPS package:
```
# pacman -Syu cups
```

Next, enable and start the CUPS service:
```
# systemctl enable cups
# systemctl start cups
```

### 2. identify the printer

Make sure the printer is connected. Run the `lsusb` command to see if the printer is detected (note: the `lsusb` command is from Arch's [usbutils](https://archlinux.org/packages/core/x86_64/usbutils/) package):
```
$ lsusb
```

### 3. install the printer driver

Different printers require different drivers, so you must find and install the appropriate driver before you can print. The packages [gutenprint](https://archlinux.org/packages/extra/x86_64/gutenprint/) and [foomatic-db-gutenprint-ppds](https://archlinux.org/packages/extra/x86_64/foomatic-db-gutenprint-ppds/) provide drivers for a wide range of printers. Otherwise, the [CUPS/Printer-spectific problems](https://wiki.archlinux.org/index.php/CUPS/Printer-specific_problems) page on the Arch Wiki has a list of drivers for other printers. Many of these drivers are installable through packages in the AUR.


### 4. configure the printer using the `lpadmin` command

To configure a printer, use the `lpadmin` command (part of the CUPS package). This command will be in the following form:
```
# lpadmin -p <queue_name> -E -v <uri> -m <model>
```

Here is an explanation of the options and how their values can be determined:
- `-p <queue_name>`: the `<queue_name>` is a name of your choice for the printer

- `-E`: this option forces encryption

- `-v <uri>`: The `<uri>` is the "Uniform Resource Identifier" of the printer, and will look something like `usb://Canon/MG6600%20series?serial=021EEB&interface=1`. It can be determined using the command `# lpinfo -v`.

- `-m <model>`: The `<model>` is a reference to the PPD file. Some examples of `<model>` values are `HL2270DW.ppd` and `gutenprint.5.3://bjc-MG6600-series/expert`. To determine availabe models, use the command `$ lpinfo -m` command (which is part of the CUPS package). This may output a lot of options, so you may want to refine the results using the `--make-and-model` option (e.g. `lpinfo --make-and-model "Canon MG6600" -m`) or use `grep` (e.g. `$ lpinfo -m | grep MG6600`).

Now you can run the `lpadmin` command. Here are some examples:
```
# lpadmin -p Canon_MG6600 -E -v "usb://Canon/MG6600%20series?serial=021EEB&interface=1" -m gutenprint.5.3://bjc-MG6600-series/expert
# lpadmin -p Brother_HL2270DW -E -v "usb://Brother/HL-2270DW%20series?serial=A3N672916" -m HL2270DW.ppd
# lpadmin -p Canon_TR4522 -E -v "usb://Canon/TR4500%20series?serial=389B25&interface=1" -m canontr4500.ppd
```

After this step, you should be able to print from the printer. So, for example, if you open a PDF and select "Print", the printer should show up. Or if you're using Gnome, the printer should show up in *Settings -> Printers*.
