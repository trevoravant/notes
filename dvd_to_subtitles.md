This document describes my experience generating subtitle files (.sub, .idx, .srt) from a DVD.

**step 1: generating the .idx and .sub files**

First, use `lsdvd` to show all tracks of the iso file:
```shell
$ lsdvd /path/to/file.iso
```
The output will show `Title` and `Subpictures`.
The subpictures represent subtitles.
Next, take your `<title>` from the `lsdvd` command, and use `mplayer` to determine the subtitles on the DVD feature:
```shell
$ mplayer -dvd-device /path/to/file.iso dvd://<title> -frames 0
```

The output should include some lines this:
```
subtitle ( sid ): 1 language: en
subtitle ( sid ): 3 language: ja
subtitle ( sid ): 5 language: unknown
number of subtitles on disk: 3
```

Next, use `mencoder` which is part of the MPlayer software (and is in the Arch Linux official repos). Run the following command to generate the .idx and .sub files:

```shell
$ mencoder -dvd-device /path/to/file.iso dvd://<title> -nosound -ovc copy -o /dev/null -sid <subtitle_id> -vobsubout my_subtitles
```

where `<title>` is the number of the title, and `<subtitle_id>` is the number of the subtitle stream from the mplayer command.

**step 2: generating the .srt file**

To generate a .srt file from the .idx and .sub files, we will use the program [vobsub2srt](https://github.com/ruediger/VobSub2SRT). The `vobsub2srt` software uses the [tesseract](https://github.com/tesseract-ocr/tesseract) optical character recognition (OCR) software to generate a .srt file. In Arch Linux, `vobsub2srt` can be installed using the [vobsub2srt-git](https://aur.archlinux.org/packages/vobsub2srt-git) AUR package, which requires the `tesseract` package to be installed (part of the Arch official repos). When installing tesseract, it asked me to select the provider for tessdata, and I chose `tesseract-data-eng`. If you also want to use another language, you have to install the appropriate package. For example, for Japanese, you have to install [tesseract-data-jpn](https://archlinux.org/packages/extra/any/tesseract-data-jpn/).

Once vobsub2srt is installed, run this command to generate the .srt file (where `my_subtitles` must match the output name in the `mencoder` command):
```shell
$ vobsub2srt --dump-images --tesseract-lang eng my_subtitles
```
The `--dump-images` flag will save the data in the .sub file to .pgm image files, which is helpful for inspection. Common mistakes from the OCR include mistaking `I` for `|`, mistaking `!` for `l`, and mistaking `I` for `l`.


**links:**  
https://www.willhaley.com/blog/dvd-subtitles/  
https://elatov.github.io/2013/09/combine-vobsub-subidx-format-subtitles-single-subrip-srt-format-file/