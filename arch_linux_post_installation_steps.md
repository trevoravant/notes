# my personal post-installation steps for Arch Linux

## wifi

- **set up WiFi connection**: install the `iwd` and `dhcpcd` packages, then start & enable `dhcpcd.service` and `iwd.service`, and finally connect to the network using the `iwctl` command

- **get the fastest mirrors**: install the `reflector` package, and (as root) run something like `# reflector --country US --sort rate --save /etc/pacman.d/mirrorlist`  
*ref*: https://wiki.archlinux.org/index.php/Reflector


## create a user with sudo privileges

1. install the `sudo` package
2. create the user and add them to the wheel group: `useradd -m trevor -G wheel`  
*ref*: https://wiki.archlinux.org/index.php/Users_and_groups#User_management 
3. set the password:`passwd trevor`
4. give sudo access to the wheel group: run `visudo` (which edits `/etc/sudoers`) and uncomment `%wheel ALL=(ALL) ALL`  
*ref*: https://wiki.archlinux.org/index.php/Sudo#Configuration


## disks

- **make a swap file**  
*ref*: https://wiki.archlinux.org/index.php/Swap#Swap_file_creation

- **add fstab entry for extra SSD**

- **make systemd timer for hard drive backup**: requires installing `rsync`


## system

- **enable magic SysReq keys (REISUB)**: make the file `/etc/sysctl.d/99-sysctl.conf` with the contents `kernel.sysrq = 1`   
*ref*: https://wiki.archlinux.org/index.php/Keyboard_shortcuts#Kernel  
*ref*: https://wiki.archlinux.org/index.php/Sysctl#Configuration

- **remove delay in GRUB wait screen**: set `GRUB_TIMEOUT=0` in `/etc/default/grub` and then re-run `grub-mkconfig -o /boot/grub/grub.cfg`  
*ref*: https://wiki.archlinux.org/index.php/GRUB#Configuration  
*ref*: https://wiki.archlinux.org/index.php/GRUB/Tips_and_tricks#Hidden_menu

- **enable autologin**: follow the steps in the link below  
*ref*: https://wiki.archlinux.org/index.php/Getty#Automatic_login_to_virtual_console

- **set up time synchronization (NTP)**: install the `ntp` package, and enable the `ntpdate.service` service file  
*ref*: https://wiki.archlinux.org/title/Network_Time_Protocol_daemon#Synchronize_time_once_per_boot

- **generate an SSH key and associate it with my Gitlab and Github accounts**: use the <code>ssh-keygen -t ed25519 -C "<i>comment</i>" -f <i>filename</i></code> command to generate keys for Gitlab, Github, and anything else  
*ref*: https://medium.com/@viviennediegoencarnacion/manage-github-and-gitlab-accounts-on-single-machine-with-ssh-keys-on-mac-43fda49b7c8d  
*ref*: https://docs.gitlab.com/ee/ssh/#ed25519-ssh-keys  
*ref*: https://docs.gitlab.com/ee/ssh/#adding-an-ssh-key-to-your-gitlab-account

- **clone my dotfiles repo to `~/code/` and symlink each file to ``/home/trevor``**: e.g. `ln -sv /home/trevor/code/dotfiles/.bashrc /home/trevor/.bashrc`

- **set default shell to zsh**: make sure the `zsh` package is installed, make sure my `~/.zprofile` is symlinked, and run `$ chsh -s /usr/bin/zsh`  
*note*: there is a `/bin/zsh` executable as well, but is symlinked to `/usr/bin/zsh` because `/bin` is symlinked to `/usr/bin`

- **audio**: (UPDATE THIS WITH YOUR EXPERIENCE FROM THE NEXT INSTALL) use `pipewire` instead of `pulseaudio`, install the `pipewire` Arch package, also install the `pipewire-pulse` Arch package which [will replace pulseaudio and pulseaudio-bluetooth](https://wiki.archlinux.org/title/PipeWire#PulseAudio_clients), and maybe install some more packages?

## desktop environment (Gnome)

* **install Gnome (X11)**: install the `xorg-xinit` package and the `gnome` package group, and edit the `~/.xinitrc` file according to the link below  
*ref*: https://wiki.archlinux.org/index.php/GNOME

* **install Gnome (Wayland)**: reference the links below (if using an Nvidia card, then see the Nvidia Arch Wiki page, you may have to remove kms from the HOOKS array in /ete/mkinitcpio.conf, enable DRM kernel mode setting, etc.)  
*ref*: https://wiki.archlinux.org/index.php/GNOME  
*ref*: https://wiki.archlinux.org/title/Wayland#Xwayland

- **automatically start Gnome at login**: make sure `~/.zprofile` or `~/.bash_profile` is filled in according to these links  
*ref*: https://wiki.archlinux.org/title/GNOME#Wayland_sessions

* **install gnome extensions (Hide Top Bar)**: to install them from Chromium, I needed to install the `gnome-browser-connector` package AND install the *GNOME Shell integration* extension for Chromium 

- **install Arc Theme**: install the `arc-gtk-theme` package, and set the `GTK_THEME` environment variable to `Arc-Dark` (e.g., put `export GTK_THEME=Arc-Dark` in ~/.zshrc or ~/.bashrc)  
*ref*: https://wiki.archlinux.org/title/GTK#Themes

- **customize top bar time and date**: go to "Settings > Date & Time" and "Tweaks > Top Bar" in Gnome (the `gnome-tweaks` package must be installed)  
*ref*: https://askubuntu.com/questions/966576/customizing-tray-taskbar-date-display-in-ubuntu-with-gnome-3

- **create keyboard shortcuts**:
  - Remove some of the default keyboard shortcuts by installing `dconf-editor` package, running the `dconf-editor` command, and then removing the keybindings in `/org/gnome/shell/keybindings/`
  - Remove some of the other predefined shortcuts by going into Settings > Keyboard > Keyboard Shortcuts and searching for one of the keys ("Super" for example)
  - Set shortcuts by going to Settings > Keyboard > Keyboard Shortcuts. Examples:  
    - move windows (using `gnome_vertical_tiler` program): <kbd>Super</kbd>+<kbd>1</kbd> or <kbd>Super</kbd>+<kbd>F1</kbd>: `/home/trevor/code/git_projects/gnome_vertical_tiler/gnome_vertical_tiler 0 20`
    - power off menu: <kbd>Super</kbd>+<kbd>Esc</kbd>: `gnome-session-quit --power-off`
    - various applications: <kbd>Super</kbd>+<kbd>t</kbd> for `kgx`, <kbd>Super</kbd>+<kbd>b</kbd> for `blender`, etc.

- **disable automatic suspend**: Settings > Power > Automatic Suspend

## Wayland

If some applications have a start delay, it may be because `gnome-keyring-daemon` is not started. To start `gnome-keyring-daemon`, make the file `~/.config/autostart/gnome-keyring-daemon.desktop` with the contents:
```
[Desktop Entry]
Type=Application
Exec=/usr/bin/gnome-keyring-daemon
Hidden=false
NoDisplay=false
X-GNOME-Autostart-enabled=true
Name[en_US]=Gnome Keyring Daemon
Name=Gnome Keyring Daemon
Comment[en_US]=Load gnome keyring daemon
Comment=Load gnome keyring daemon
```
*ref*: https://bbs.archlinux.org/viewtopic.php?id=180075  
*ref*: https://bugzilla.gnome.org/show_bug.cgi?id=729101

## applications

- **install various packages & groups**: `man-db`, `tree`, `git`, `gvim`, `okular`, `tmux`, `texlive`, `nvidia`, `python-pytorch-cuda`, `xdotool`, `xclip`, `chromium`, `vlc`, `libreoffice-still`, `gnumeric`, `blender`, `gimp`, `inkscape`, `gnome-tweaks`

- **install various AUR packages**: note that installing a package from the AUR requires the `base-devel` package group to be installed

- **install dropbox from the AUR**: You may need to get the pgpkey, which should be mentioned in the `PKGBUILD` (otherwise you may get the error you may get the error `ERROR: One or more PGP signatures could not be verified!`). To get the key, run something like `gpg --keyserver pgp.mit.edu --recv-key <key-id>`. Also, don't forget to modify the `~/.dropbox-dist` folder as mentioned in the ArchWiki.    
*ref*: https://wiki.archlinux.org/index.php/Arch_User_Repository#Acquire_a_PGP_public_key_if_needed

- **disable Chromium's "*Choose password for new keyring*" prompt**: make the `chromium` command always use the `--password-store=basic` flag by making the file `~/.config/chromium-flags.conf` with the contents `--password-store=basic`  
*ref*: https://wiki.archlinux.org/title/Chromium#Making_flags_persistent

- **install vim plugins**: install `vim-plug` by downloading `plug.vim` and putting it in `~/.vim/autoload`, install plugins using `:PlugInstall`, then build `YouCompleteMe` (depending on what packages are already installed, this may require installing the following packages: `base-devel`, `cmake`, `npm`, `go`, `gcc`)

- **install pip and python packages**: install `python-pip` package, and then install packages using <code>pip install --user <i>package_name</i><code>

- **install some fonts**: Arch packages: `ttf-opensans`, `adobe-source-code-pro-fonts`


## other

- **get bluetooth working**
