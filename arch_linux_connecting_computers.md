This write-up describes several ways to connect computers (e.g., to transfer files) in Arch Linux.

## connecting two computers through a router over Wifi

Make sure the [nmap](https://archlinux.org/packages/extra/x86_64/nmap/) Arch Linux package is installed on Computer 1. Make sure Wifi is disabled on the Computer 2 (e.g., the computer is off, or is in airplane mode). Then run the following command on the Computer 1 to do a host discovery:
```bash
$ nmap -sn 192.168.1.2-20
```
Start the Computer 2, and run the command above again on Computer 1. You should see a new entry, which tells you the IP address of Computer 2 (e.g., `192.168.1.9`). Next, start the OpenSSH server daemon on Computer 2 (which requires having the [openssh](https://archlinux.org/packages/core/x86_64/openssh/) Arch Linux package installed):
```
# systemctl start sshd
```

You should now be able to connect to the Computer 2 from Computer 1 using `ssh`:
```bash
$ ssh trevor@192.168.1.9  # or whatever the username and IP address are
```
Or use `scp` to transfer files.


## connecting two computers using an ad-hoc network (!!!WORK IN PROGRESS!!!)

This document explains how to create an ad-hoc network to connect two computers over Wi-Fi in Arch Linux. Run the following commands on both computers:
```
# systemctl start sshd
# ip link set wlan0 down
# iw wlan0 set type ibss
# ip link set wlan0 up
# iw wlan0 ibss join trevortest 2412
# ip address add 192.168.1.X/24 dev wlan0
```
Where `X` in the last command should be a different number for each computer (e.g., `1` on the first computer and `2` on the second computer).

You should now be able to do things like log into one computer from the other, and copy files between the computesrs. Examples are given in the following commands:
```
$ ssh trevor@192.168.1.1
$ scp trevor@192.168.1.1:Documents/file.txt /home/trevor
$ rsync -avzP trevor@192.168.1.1:Documents/file.txt /home/trevor
```
Note that on my computers I am only able to get a max file transfer rate of about 2MB/s, but I am trying to figure out how to increase it.


## connecting two computers using a USB-to-USB cable

It is theoretically possible to connect two computers using a cable running between the USB3 ports on each computer (see [this](https://unix.stackexchange.com/a/313569/294686)). However, after some research, I do not believe there is currently an easy way to do it (see [this](https://bbs.archlinux.org/viewtopic.php?id=251030)).
