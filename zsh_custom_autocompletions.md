This document describes my experience making custom autocompletions for zsh.


### autocompletion functions path

Zsh searches for autocompletion functions in directories in the `fpath` variable.
Files which contain these functions must begin with an underscore.
You can modify the `fpath` variable in your `~/.zshrc` file, but changes must be done prior to the `compinit` command.
So, in your `~/.zshrc` file, add something like this:
```zsh
fpath=(/path/to/directory/ $fpath)
```

### ignore braces in function

```
setopt ignore_braces
compadd -S _todo foo hi/bar/{a,b}, blah
unsetopt ignore_braces
```


## simple examples

### simple example - generate an autocomplete list of `a`, `b`, and `c` as well as files in the directory

```zsh
#compdef myfun
                               
_myfun() {
  _alternative 'arguments:custom arg:(a b c)' 'files:filename:_files'

}
```

### simple example - autocomplete arguments

Suppose I have a function named `myfun`.
I want zsh to autcomplete two types of arguments for the `myfun` function: `-s` ("simple option") and `-c` ("crazy option").
I can create the following file called `_myfun` that is in a path in `fpath`, and then.
```zsh
#compdef myfun
                               
_myfun() {
  _arguments '-s[simple option]' '-c[crazy option]'
}
```

Now, if I open a new zsh shell, type `myfun` and a space, and then type <kbd>Tab</kbd>, it should autocomplete a dash, and then on another <kbd>Tab</kbd>, it should show this:
```
% myfun -
-c  -- crazy option
-s  -- simple option
```


### simple example - complete mp3 file names

This function will autocomplete all mp3 names in the current directory.
To use it, navigate to a directory with mp3 files in it, type `myfun` and space, and then <kbd>Tab</kbd>.

```zsh
#compdef myfun

_myfun() {
  _arguments -C '1:cmd:->cmds' 
  case "$state" in
      (cmds)
          local -a music_files
          music_files=( *.mp3 )
          _multi_parts / music_files
          ;;
  esac
}
```

*refs:*  
https://zsh.sourceforge.io/Doc/Release/Completion-System.html#Completion-System  
https://github.com/zsh-users/zsh-completions/blob/master/zsh-completions-howto.org
