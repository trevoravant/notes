This document describes my experience making custom autocompletions for zsh.


### autocompletion files and functions

Rules for autocompletion are stored in a collection of files.
Each file's name must begin with an underscore.
The name of this file can be anything, but it usually makes the most sense to name the file after the associated shell command (i.e., a file named `_diff` for the shell command `diff`).
The first line of the file will be `#compdef <name>` where `<name>` is the shell command name.
Additionally, the same completion can be applied to multiple shell commands by specifying multiple names as follows: `#compdef <name1> <name2> <name3>`.
The most basic file would like something like the following example, which sets autocomplete for the `ls` command to have only three options: `a`, `b`, and `c`:
```zsh
#compdef ls
compadd a b c
```

### autocompletion files path

Zsh searches for autocompletion functions in directories in the `fpath` variable.
Multiple completion functions can exist for the same shell command, but the function appearing earlier in the `fpath` will take precedence. 
You can modify the `fpath` variable in your `~/.zshrc` file, but changes must be done prior to the `compinit` command.
So, in your `~/.zshrc` file, add something like this:
```zsh
fpath=(/path/to/directory/ $fpath)
```


### ignore braces in function

```
setopt ignore_braces
compadd -S _todo foo hi/bar/{a,b}, blah
unsetopt ignore_braces
```


## simple examples

### simple example - generate an autocomplete list consisting of `a`, `b`, and `c`

```zsh
#compdef myfun
                               
compadd a b c
```


### simple example - complete all folders in `/home/trevor/docs/`, except for folders `alpha` and `beta`

```zsh
#compdef myfun

# the command below puts the outputs of the ls command into an array,
# the -Istuff flag will exclude the folder named "stuff"
folder_names=( $( ls -I{alpha,beta} /home/trevor/docs/ ) )


compadd $folder_names
```


### simple example - different names than completions

The following shows how to give different *displayed* names of completions than completions themselves.
However, you must type the completion names rather than displayed names to get an autocomplete.
```zsh
#compdef zz

local -a _descriptions _values

_descriptions=( 'a|' 'b?!' 'c}')

_values=( 'a' 'b' 'c')

compadd -d _descriptions -a _values
```


### simple example - autocomplete arguments

Suppose I have a function named `myfun`.
I want zsh to autcomplete two types of arguments for the `myfun` function: `-s` ("simple option") and `-c` ("crazy option").
I can create the following file called `_myfun` that is in a path in `fpath`, and then.
```zsh
#compdef myfun
                               
_myfun() {
  _arguments '-s[simple option]' '-c[crazy option]'
}
```

Now, if I open a new zsh shell, type `myfun` and a space, and then type <kbd>Tab</kbd>, it should autocomplete a dash, and then on another <kbd>Tab</kbd>, it should show this:
```
% myfun -
-c  -- crazy option
-s  -- simple option
```


*refs:*  
https://zsh.sourceforge.io/Doc/Release/Completion-System.html#Completion-System  
https://github.com/zsh-users/zsh-completions/blob/master/zsh-completions-howto.org
