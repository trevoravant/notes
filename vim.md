This document contains some notes for Vim.


### using ctags

In a shell, go into the directory of your project and run `$ ctags -R`.
This will generate a file named `tags`.
In Vim, go to a file in your project, and make sure Vim's current directory is set as the directory where the `tags` file is (i.e. in Vim, do `:cd /path/to/directory`).
If you aren't in this directory, you will get a `No tags file` warning.
Move your cursor to a function name and press <kbd>Ctrl</kbd>+<kbd>]</kbd>, which will bring you to the declaration of that function.
To go back to where you were, press <kbd>Ctrl</kbd>+<kbd>T</kbd>.


### using cscope

In a shell, generate a list of all files by running `$ find -name *.cpp -o -name *.hpp > cscope.files`.
Then run, `$ cscope -q - R -B -i cscope.files` to generate the cross-reference file `cscope.out`.
Then run, `$ cscope -d` to start the cscope interactive shell.
To exit the cscope interactive shell, press <kbd>CTRL</kbd>+<kbd>D</kbd>.
In Vim, to make <kbd>CTRL</kbd>+<kbd>]</kbd> use cscope instead of ctags, do `:set cscopetag`.
To use ctags instead of cscope, do `:set nocscopetag`.


### using clangd via YouCompleteMe

These instructions are mostly based off of the [YouCompleteMe readme](https://github.com/ycm-core/YouCompleteMe?tab=readme-ov-file#c-family-semantic-completion).

1. (I don't think this is necessary when using the system-wide clangd executable) install YouCompleteMe with the flag `--clangd-completer` (or the `--all` flag)

2. (may or may not be necessary) add this line to your .vimrc to specify your system's clangd executable (otherwise YouCompleteMe will use a version that was installed in `~/.vim/vimplugplugs/YouCompleteMe/`):

    `let g:ycm_clangd_binary_path='/usr/bin/clangd`

    *note:* adding this line has been necessary sometimes to make everything work

3. (optional) add one of these two lines to your .vimrc to make clangd show more info in `:YcmToggleLogs` > `clangd_stderr*.log`:  
`let g:ycm_langd_args=['-log=verbose']`  
`let g:ycm_langd_args=['-log=verbose','-pretty']`

4. generate the file `compile_commands.json` when running the cmake configure command, which can be done in two ways:
   - run the cmake configure command with this option:  
     `-DCMAKE_EXPORT_COMPILE_COMMANDS=ON`

   - add this line to your CMakeLists.txt
     `set(CMAKE_EXPORT_COMPILE_COMMANDS ON)`

     *note:* sometimes I have had to use `ON CACHE INTERNAL ""` in place of `ON` ([ref](https://stackoverflow.com/a/66560758/9357589))

5. copy or symlink the generated `compile_commands.json` file to the root or the project

6. now you should be able to open a file in your project, and do things like:  
`:YcmCompleter GoToDeclaration`  
`:YcmCompleter GoToDefinition`  
`:YcmCompleter GoToReferences`

Notes:

- the following Vim commands come in handy for debugging: `:YcmDebugInfo`, `:YcmDebugInfo`, `:YcmDiags`
- I have not had to use a `.ycm_extra_conf.py` file, and it is not recommendend in most cases to use one ([ref](https://stackoverflow.com/a/72558653/9357589))
- after doing `GoToDefinition` or `GoToDeclaration`, you can use <kbd>Ctrl</kbd>+<kbd>O</kbd> to go back to where you where before

### example: install Vim from source using the rockylinux:8 image

Note that in Rocky Linux 8, Vim can be installed with `$ dnf install vim`.
However, this installs Vim version 8.0 (too old for YouCompleteMe) does not have the `clientserver` feature.

#### required packages

The Rocky Linux packages needed for installing from source are shown in the table below:

| Package | Reason Needed |
| --- | --- |
| `gcc` | or get the error "error: no acceptable C compiler found in $PATH" |
| `make` | or get the error "make: command not found" |
| `ncurses-devel` | or get the error: "You need to install a terminal library; for example ncurses." |
| `diffutils` | for the diff command, or get warning during complilation |
| `findutils` | for the find command, or get warning during complilation |
| `libXt-devel` (optional) | sometimes required for `--with-x=yes` option |
| `python39` (optional, or another Python3 package) | for python3interp (`:py3` Vim command) |
| `python39-devel` (optional, or another Python3 package) | for python3interp (`:py3` Vim command) |


#### options

| Option                     | Description |
| ---                        | ---         |
| `--with-x=` | use the X Window system, needed for +clientserver option, must be set to normal, big, or huge for clientserver option |
| `--with-features=` | use the X Window system, needed for +clientserver option |
| `-enable-gui=` | which X11 GUI to use (not sure if this makes difference in many of my builds) |
| `--prefix=` | where to install architecture-independent files (seems to default to `/usr/local`) |
| `--enable-multibyte` | include support for multibyte characters, which are characters that use more than one byte for each character (e.g. Chinese, Japanese, or Korean characters) |
| `--enable-cscope` | include support cscope interface |
| `--enable-python3-interp` | allows you to use the `:python3` or `:py3` command from the Vim command prompt |
| `--with-python3-command=` | name of the Python 3 command (e.g., python3.9), will default to python3 if not specified |
| `--with-python3-config-dir=` | option is deprecated (see `src/auto/configure` in the Vim source code) |


#### YouCompleteMe

Per the installation instructions, YouCompleteMe requires at least Vim 9.1.0016+.
Also, the installation instructions say that in Ubuntu, you should install `build-essential`,  `cmake`, and `python3-dev`.
Packages I needed to install are shown in the table below:

| Package | Reason Needed |
| --- | --- |
| `gcc-c++` | or get the error "No CMAKE_CXX_COMPILER could be found." |
| `cmake` | or get the error "Unable to find cmake executable ..." |
| `python39` | Python3.8 and earlier didn't work |
| `python39-devel` | or get the error "Python headers are missing ..." |

Run this command to insall YouCompleteMe:
`$ python3.9 install.py`

You have to install additional packages if you were to do `python3.9 install.py --all`.


#### vim send

Open two terminals.
Run this in both terminals `$ xhost + local:root`.
You must add the options `-v /tmp/.X11-unix:/tmp/.X11-unix` and `-e DISPLAY=$DISPLAY` must be addeded to the docker command.
Run this on the first terminal:  
`# docker container run -it --rm -v /tmp/.X11-unix:/tmp/.X11-unix -e DISPLAY=$DISPLAY --name mycont im_vim /bin/bash`
Run this in the second terminal:  
`# docker exec -it mycont /bin/bash`

| Terminal 1 | Terminal 2 |
| --- | --- |
| `$ xhost + local:root` <br> `# docker container run -it --rm -v /tmp/.X11-unix:/tmp/.X11-unix -e DISPLAY=$DISPLAY --name mycont im_vim /bin/bash` | `$ xhost + local:root` <br> `# docker exec -it mycont /bin/bash` |


#### full Dockerfile:

```dockerfile
FROM rockylinux:8
                               
# install preliminary packages
USER root
RUN dnf -y install sudo
RUN dnf -y install git
RUN dnf -y install curl

# add a user
RUN useradd user
RUN usermod -G wheel user
RUN echo 'user:password' | chpasswd

# install vim
RUN dnf -y install gcc
RUN dnf -y install make
RUN dnf -y install ncurses-devel
RUN dnf -y install diffutils
RUN dnf -y install findutils
RUN dnf -y install libXt-devel
RUN dnf -y install python39
RUN dnf -y install python39-devel
RUN git clone --branch v9.1.0016 --single-branch https://github.com/vim/vim.git
WORKDIR /vim/
RUN ./configure --with-features=normal \
                --enable-multibyte \
                --with-x=yes \
                --enable-python3interp \
                --enable-cscope
RUN make VIMRUNTIMEDIR=/usr/local/share/vim/vim91
USER root
RUN make install

# vimplug & YouCompleteMe
RUN dnf -y install gcc-c++
RUN dnf -y install cmake
USER user
RUN mkdir -p /home/user/.vim/autoload/
RUN curl -fLo ~/.vim/autoload/plug.vim --create-dirs https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim
RUN echo "call plug#begin('~/.vim/vimplugplugs')" >> /home/user/.vimrc
RUN echo "Plug 'Valloric/YouCompleteMe'" >> /home/user/.vimrc
RUN echo "call plug#end()" >> /home/user/.vimrc
RUN vim -c ":PlugInstall | :q | :q"
WORKDIR /home/user/.vim/vimplugplugs/YouCompleteMe/
RUN python3.9 install.py

# final stuff
WORKDIR /home/user/
CMD
```
