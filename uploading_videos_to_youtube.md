These are some notes regarding uploading videos to Youtube.

# quality

Youtube provides several video quality options that a given video can be be played at, e.g. 360p, 480p, 720p, 1080p. The number before "p" denotes the number of vertical pixels (e.g. 480p has 480 vertical pixels). After you upload a video, Youtube will set the max quality of the video to the preset less than or equal to the number of vertical pixels in the video. For example, if you upload a video with 500 vertical pixels, since it 500 is between 480p and 720p, Youtube will give that video a max quality of 480p.


# settings

### https://support.google.com/youtube/answer/1722171

When uploading a video, Youtube has several recommended settings shown in the link above. Below is a table showing how to use ffmpeg to apply some of these settings. Also see [this page](https://gist.github.com/mikoim/27e4e0dc64e384adbcb91ff10a2d3678).


| setting | ffmpeg option | ffmpeg doc | notes  |
| :-- |:-- | :-- | :-- |
| moov atom at the front of the file (Fast Start) | `-movflags faststart` | [*](https://ffmpeg.org/ffmpeg-formats.html#Options-9) | <ul><li>to check if the moov atom is at the beginning or end of the file, see the technique [here](https://stackoverflow.com/a/56963953/9357589)</li></ul> |
| H.264 video codec | `-c:v libx264` | [*](https://ffmpeg.org/ffmpeg-codecs.html#libx264_002c-libx264rgb) |
| high profile | (seems to be default with libx264)<br>`-profile:v high` | | <ul><li>the profile level will manifest itself as something like `profile High` in the output of the ffmpeg command</li><li>[this page](https://www.rgb.com/h264-profiles) has some info about profiles</li></ul> |
| 2 consecutive B-frames | `-bf 2` | [*](https://ffmpeg.org/ffmpeg-codecs.html#Codec-Options) | |
| closed GOP | (default with libx264)<br>`-x264-params open_gop=0` | | <ul><li>GOP = [Group Of Pictures](https://en.wikipedia.org/wiki/Group_of_pictures)<li>this setting will manifest itself as the `open_gop=` parameter in the output of the ffmpeg command</li></ul> |
| GOP half the framerate | `-g <half_framerate>` | [*](https://ffmpeg.org/ffmpeg-codecs.html#Codec-Options) | <ul><li>a GOP begins with a I-frame (AKA keyframe) and is followed by several P and B-frames</li><li>this setting will manifest itself as `keyint=` in the output of the ffmpeg command</li><li>see [this answer](https://stackoverflow.com/a/37285229/9357589) for a technique to calculate the GOP sizes of a video</li></ul> |
| CABAC | (default with libx264)<br>`-coder ac` | [*](https://ffmpeg.org/ffmpeg-codecs.html#toc-Options-34) | <ul><li>CABAC = [Context-Adaptive Binary Arithmetic Coding](https://en.wikipedia.org/wiki/Context-adaptive_binary_arithmetic_coding)</li><li>whether or not CABAC is enabled will manifest itself as the `cabac=` parameter in the output of the ffmpeg command</li></ul> |
| chroma subsampling: 4:2:0 | (seems to be default with libx264)<br>`-pix_fmt yuv420p` (8-bit, standard)<br>*or*<br>`-pix_fmt yuv420p10le` (10-bit) | | <ul><li>the chroma subsampling can be determined because ffmpeg will print a line such as `[libx264 @ 0x559455490f80] profile High, level 3.0, 4:2:0, 8-bit`</li></ul> |
| no interlacing | ONLY USE THIS IF THE VIDEO IS INTERLACED!!!<br>`-vf "yadif"` | [*](https://ffmpeg.org/ffmpeg-filters.html#yadif-1) | <ul><li>`yadif` = Yet Another DeInterlacing Filter</li><li>note: many VOB files from DVDs are interlaced</li></ul>  |
| BT.709 colorspace<br>(SDR uploads) | <ul><li>`-color_primaries 1`</li><li>`-color_trc 1`</li><li>`-colorspace 1`</li></ul> | [*](https://ffmpeg.org/ffmpeg-codecs.html#Codec-Options) | <ul><li>Youtube [recommends](https://support.google.com/youtube/answer/1722171?hl=en) BT.709 for the Color Space, Color Transfer Characteristics (TRC), Color Primaries and Color Matrix Characteristics</li><li>code based on [this answer](https://stackoverflow.com/a/37291118/9357589)</li></ul> |
|  AAC-LC audio codec & sample rate 96kHz or 48kHz | `-c:a aac -ar 48k` (may or may not be default) | [*](https://ffmpeg.org/ffmpeg-codecs.html#aac) | <ul><li>AAC-LC = [Advanced Audio Coding - Low Complexity](https://en.wikipedia.org/wiki/Advanced_Audio_Coding)</li><li>when running the ffmpeg, the audio information will appear in a line such as `Stream #0:1: Audio: aac (LC) (mp4a / 0x6134706D), 48000 Hz, stereo, fltp, 128 kb/s`</li><li>since Youtube will re-encode the audio anyway, you may want to convert the source audio using a high bit rate, e.g., `-b:a 320k`</li></ul> |

