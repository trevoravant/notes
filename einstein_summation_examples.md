| input 1          | input 2          | output             |code               | interpretation |
| :--              |:--               | :--                | :--               | :--            |   
| $(m,n_{vec})$    | $(m,n_{vec})$    | $(n_{vec})$        | ij,ij -> j     | dot product of each $m$-sized vector in input 1 with corresponding $m$-sized vector in input 2 |  
| $(n,m,n_{mat})$  | $(m,p,n_{mat})$  | $(n,p,n_{mat})$    | ijn,jkn -> ikn | multiply each $n{\times}m$ matrix by each $m{\times}p$ matrix |
