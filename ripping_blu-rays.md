WORK IN PROGRESS

This document describes my experience ripping video files from a Blu-ray disc.

## Blu-ray files

The raw files video files on a Blu-ray disc seem to be in the directory `BDMV/STREAM` and seem to be in the format `.m2ts`. However, the files do not seem to be easily playable.

## encryption

Blu-rays have a more advanced encryption than DVDs, and are therefore harder to rip. Blu-rays use the Advanced Access Content System (AACS)

Find VUK database

## making an ISO

I have been able to make an ISO of Blu-ray discs by simply using the `dd`` command:
```
$ dd if=/path/to/bluray/device of=/path/to/file.iso
```
On my computer, `/path/to/bluray/device` is usually `/dev/sr0`.

## MakeMKV

**overview**

MakeMKV is a program that transfers an [exact, lossless copy](https://forum.makemkv.com/forum/viewtopic.php?t=24388) of the content on a Blu-ray to an MKV video file. It can be installed in Arch Linux as a commandline-only program ([makemkv-cli](https://aur.archlinux.org/packages/makemkv-cli)) or a program with a GUI ([makemkv](https://aur.archlinux.org/packages/makemkv)). I usually use the commandline-only version.

**beta key**

MakeMKV is currently in beta and is free, but you need the current beta key to use it. If you don't have the correct key, MakeMKV may give you a message that says something like:
```
Evaluation period has expired, shareware functionality unavailable.
Evaluation period has expired. Please purchase an activation key if you've found this application useful. You may still use all free functionality without any restrictions.
Failed to open disc
```
In this case, you have to get the current beta key (which is usually linked to from the Arch AUR packages websites). Then open the file `~/.MakeMKV/settings.conf` and add this line:
```
app_Key = "<KEY>"
```

**sg kernel module**

Sometimes makemkv gives me the message *The program can't find any usable optical drives.* I fixed this problem by enabling the *sg* kernel module:
```
# modprobe sg
```

**making MKV files**

To save all videos on a Blu-ray disc as MKV files, insert the disc into your Blu-ray player, and run the following command:

```bash
makemkvcon mkv disc:0 all /path/to/save/directory/
```
If you have an ISO of the Blu-ray on your hard drive, then I think you should be able to replace `disc:0` with `iso:/path/to/file.iso`, but I haven't gotten that command to work yet.

**making a non-decrypted copy of the disc on your hard drive**

Although you can make an ISO of the disc, MakeMKV cannot play that ISO. The only way I know how to use MakeMKV to make an accessible copy of the disc on your hard drive is to make a copy the folder structure of the disc, and copy the `discatt.dat` file to that folder. Here are the instructions, which I determined from [this link](https://forum.makemkv.com/forum/viewtopic.php?f=12&t=17087):
* unpack the ISO into folder 1
* make a full disc backup into folder 2 (using the command `makemkvcon backup disc:0 /path/to/folder2`) and the moment MakeMKV starts copying files, abort the command
* now you should have a file called `discatt.dat` in folder 2, so copy it into folder 1
* now that folder 1 has the `discatt.dat` file, it can be opened with MakeMKV (for example, to generate MKV files from folder 1, use this command: `makemkvcon mkv /path/to/folder1 all /path/to/save/mkv/files`)


## Youtube uploads

**audio**

Some MKV files will have 7.1 surround sound. [Youtube suggests](https://support.google.com/youtube/answer/1722171) using "stereo" or "stereo+5.1" surround sound. To convert to 5.1 in ffmpeg, you can use the `-ac` option. For example, to convert to 5.1, you should specify six channels, and use the option `-ac 6`.

**common resolutions**

| DAR  | 480p     | 720p    | 1080p     | 1440p     | 2160p |
| :--  |:--       | :--     | :--       | :--       | :--   |
| 16:9 | ≈854x480 | 1280x720| 1920x1080 | 2560x1440 | 3840x2160 |


**MKV file with progressive scan, DAR=16:9, 5.1 or 7.1 surround sound**
``` bash
ffmpeg -i video.mkv -vf "scale=3840x2160:flags=lanczos,setsar=1" -c:v libx264 -x264-params open_gop=0 -profile:v high -crf 10 -pix_fmt yuv420p -bf 2 -g 15 -coder ac -color_primaries 1 -color_trc 1 -colorspace 1 -movflags faststart -c:a aac -b:a 1G -ac 6 video.mp4
```

* with stereo audio: `-c:a aac -b:a 1G`


## references

https://wiki.archlinux.org/title/Blu-ray
