* axis-angles
* exponential coordinates
* rotation matrices
* quaternions  
Euler parameters  
Euler-Rodrigues parameters  
Euler-Rodrigues symmetric parameters  
Euler symmetric parameters
* exponential coordinates
* Rodrigues parameters  
Gibbs vector  
Euler-Rodrigues vector  
Cayley-Rodrigues parameters  
Rodrigues vector
* modified Rodrigues parameters
* Euler angles
* Cayley-Klein paramteters

**references:**

[A Survey of Attitude Representations by Shuster](http://malcolmdshuster.com/Pub_1993h_J_Repsurv_scan.pdf)