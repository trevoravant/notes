# recovering Arch Linux after you broke it

This document describes how to recover a broken Arch Linux installation. For example, it might be broken after you changed a configuration file.

1. obtain an [Arch Linux USB installation drive](https://wiki.archlinux.org/title/USB_flash_installation_medium)
2. insert the USB drive, boot the computer, enter the BIOS, and select the USB drive as the boot drive
3. determine the boot, root, home, and possibly other partitions of the system you're trying to restore by running `$ lsblk`. For example, on my I have a 512M boot partition `/dev/nvme0n1p1`, a 60G root partition `/dev/nvme0n1p2`, and a 416G home partition `/dev/nvme0n1p3`.
4. mount the root partition: `# mount /dev/nvme0n1p# /mnt`
5. mount the boot partition (the mount location may vary and can be determined by looking at your `/etc/fstab` file):  
`# mount /dev/nvme0n1p# /mnt/boot`  
or  
`# mount /dev/nvme0n1p# /mnt/boot/efi`  
or something else
6. (if needed) mount the home partition: `# mount /dev/nvme0n1p# /mnt/home`
7. arch-chroot into the the system: `# arch-chroot /mnt`
8. make the necessary changes
9. exit the arch-chroot: `# exit`
10. power off: `# poweroff`


**references**

* https://joshtronic.com/2017/09/07/fixing-an-arch-linux-system-that-is-booting-into-emergency-mode/
