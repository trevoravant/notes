This document describes methods to use a proxy server in Linux.


### Method 1: ProtonVPN and WireGuard

Note that ProtonVPN offers a free account.

1) install the `openvpn`, `wireguard-tools`, and `systemd-resolveconf` Arch Linux packages

2) go to your protonvpn.com account and download a WireGuard .conf file

3) move the .conf file into `/etc/wireguard/`

4) start/enable `systemd-resolved`

5) run `# wg-quick up <config file>` to start and `# wg-quick down <config file>` to stop  

*ref*: https://wiki.archlinux.org/title/ProtonVPN
