# how to make file links clickable in the terminal

## opening file links in Vim

Any text file can be opened in Vim.
However, if you want to open a file in a pre-existing instance of Vim and not create a new instance, you can use a server.
To use a server in Vim, your Vim installation must be compiled with the `+clientserver` option.
To start a server named "VIM" in Vim, you can run Vim as

```sh
$ vim --servername VIM
```

With Vim running as a server, you can jump to *line_number* of a tex file with this shell command:
```sh
$ vim --remote +line_number /path/to/file.tex
```


## making `vim --remote` an application option in your desktop environment (i.e., creating a .desktop file)

To create a custom application which runs `vim --remote` and which can open files from a file browser, you can create a .desktop file.
Create the following .desktop file, which will open a file in the "VIM" Vim server if that server exists, and will otherwise open in it in a new Vim window:
```sh
$ cat ~/.local/share/applications/vimremote.desktop
[Desktop Entry]
Name=Vim Remote
GenericName=Text Editor
Comment=Edit text files
TryExec=vim
Exec=sh -c "f=%f; if vim --serverlist | grep -q VIM; then vim --remote $f; else gtk-launch vim file:$f; fi"
Terminal=false
Type=Application
Icon=gvim
StartupNotify=false
```

This will add an entry called "Vim Remote" to the application selection menu when right-clicking a file and selecting *Open With...*
If the entry does not appear, you can run the following command to force update the application list:
```sh
$ xdg-desktop-menu forceupdate
```

You can also check the correctness of your desktop by running
```sh
$ desktop-file-validate ~/.local/share/applications/vimremote.desktop
```


## shell commands which can generate hyperlinks

Many shell commands do not generate clickable links by default.
However, some have have options to generate hyperlinks:

| Standard Command | Hyperlink Command |
| ---              | ---              |
| `ls`             | `ls --hyperlink` |


## terminals

### general info

Many terminals do not render file names as clickable.
For example, file names printed by the `ls` command will not be clickable in most terminals. 
However, many terminals support clickable file links if the file name is formatted as a Uniform Resource Identifier (URI).
A file URI has the following format:
```
file://host/path
```

The `host` field can be left out, but the corresponding slashes cannot.
So for example, a file URI may look like `file:///home/trevor/file.txt`.
If you run the command below, the output will usually be clickable, and will open with the default application witha left-click or control-left-click:
```sh
$ echo file:///home/trevor/file.txt
file:///home/trevor/file.txt
```


### konsole

Some terminals can automatically create clickable links for file names displayed in the terminal. The main terminal I'm familiar with is `konsole`. To figure how the files are opened when clicked, go to *Settings > Configure Konsole...*, then create a profile if one doesn't exist, then go to *Edit... > Mouse > Miscellaneous > Text Editor Command: > Edit* and enter the following:
```sh
sh -c "if vim --serverlist | grep -q VIM; then vim --remote +LINE PATH; else kgx -- vim +LINE PATH; fi"
```

## custom filetypes

We will now create a custom file type `.lsh` which we will configure to be opened using a custom application.

## 1) make a new MIME type for .lsh files

To make a new MIME (Multipurpose Internet Mail Extensions) type that associates any file that ends in `.lsh`, make this file (the directory may have to be created):
```xml
$ cat ~/.local/share/mime/packages/lsh.xml
<?xml version="1.0" encoding="UTF-8"?>
<mime-info xmlns="http://www.freedesktop.org/standards/shared-mime-info">
  <mime-type type="text/lsh">
    <comment>Trevors special file</comment>
    <glob pattern="*.lsh"/>
  </mime-type>
</mime-info>
```
Then run
```sh
$ update-mime-database ~/.local/share/mime
```
A `.lsh` file should now be recognized as type `text/lsh` instead of `text/plain`:
```sh
$ xdg-mime query filetype file.lsh
text/lsh
```

*ref*: https://wiki.archlinux.org/title/XDG_MIME_Applications  
*ref*: https://unix.stackexchange.com/a/208502/294686

## 2) create a custom .desktop file to open .lsh files

Now make the following .desktop file:
```sh
$ cat ~/.local/share/applications/sh.desktop
[Desktop Entry]
Name=Sh
GenericName=Text Editor
Comment=Edit text files
TryExec=vim
Exec=sh -c '%F'
Terminal=false
Type=Application
Icon=gvim
StartupNotify=false
```
This will add an entry called "Sh" to the application selection menu when right-clicking a file and selecting *Open With...*
Next, right-click a `.lsh` file, select *Open With...*, and choose the *Sh* application, and select *Always use for this file type* to associate `.lsh` files with the `Sh` application.

## 3) replace the filepaths of the output of a python file with links

Finally, the following shell script will help us convert output of Python command such that every Python error file is replaced with a link of the same name linked to a `.lsv` file.
If you have set up parts 1 and 2 correctly, then clicking on that link should open the lsv file which will open the Python file in Vim at the correct line number.
```sh
$ cat link.sh
#!/bin/zsh 
# for python files             
# 
# applied as follows:          
# 
# $ python file.py |& ./link.sh 
# 
# assumes the error output lines look like this: 
# 
#  File "/home/trevor/Downloads/py_test/test.py", line 7, in <module> 
# 
# so the default regex pattern will look something like this: 
# 
#  File "[a-zA-Z0-9_\/\.\-]*\.py", line [0-9]*, in .* 
# 
# note: use the "-u" option in the python command to allow this script to work in real time 
# 
while IFS= read line 
do 
    if echo "$line" | grep -q '  File "[a-zA-Z0-9_\/\.\-]*\.py", line [0-9]*, in .*'; then 
        # get python file path and line number 
        filepath=$(echo $line | sed 's/  File "\([a-zA-Z0-9_\/\.\-]*\.py\)", line [0-9]*, in .*/\1/') 
        num=$(echo $line | sed 's/  File "[a-zA-Z0-9_\/\.\-]*\.py", line \([0-9]*\), in .*/\1/') 
        # create lsh file path 
        lsh=$filepath 
        lsh="${lsh:1}" 
        lsh="${lsh//\//-}" 
        lsh="${lsh//./-}" 
        lsh=$lsh-$num 
        lsh=/tmp/$lsh.lsh 
        touch $lsh 
        chmod +x $lsh 
        # create link 
        pre='  File "' 
        post=$(echo $line | sed 's/  File "[a-zA-Z0-9_\/\.\-]*\.py\(", line [0-9]*, in .*\)/\1/') 
        link='\e]8;;file://'$lsh'\e\\'$filepath'\e]8;;\e\\' 
        line_link=$pre$link$post 
        echo $line_link 
    else 
        echo $line 
    fi 
done 
# fill lsh file 
echo \ 
"if vim --serverlist | grep -q VIM; then 
  vim --remote +"$num $filepath" 
else 
  kgx -- sh -c 'vim +"$num $filepath"' 
fi"\ 
> $lsh
```
Now you can run:
```sh
$ python file.py |& ./link.sh
```
which should output the same thing as `$ python file.py` but with the file names as clickable links.
A similar method can be used for other file types.

*ref*: https://askubuntu.com/a/1391072/827176


## tmux method

This problem can also be approached with `tmux`.
Using a custom shell script and a tmux command, tmux can be configured so that the file is opened to the corresponding line number in Vim using a double-click.
This method does not actually turn the filenames into clickable links, but achieves the same effect.

First, create the following shell script:
```sh
$ cat open_filepath.sh
# extract filepath and linenum if found, then open filepath to linenum
open_file () {
	filepath=$(echo $filepath_and_linenum | cut -d ' ' -f 1)
	linenum=$(echo $filepath_and_linenum | cut -d ' ' -f 2)
	vim_cmd='if vim --serverlist | grep -q VIM; then vim --remote +'$linenum' '$filepath'; else kgx -- vim +'$linenum' '$filepath'; fi'
	eval ${vim_cmd}
	exit 0
}

# globalpath:linenum:columnnum
filepath_and_linenum=$(echo $1 | sed -n 's/^\(\/[a-zA-Z0-9_\/\.\-]\+\):\([0-9]\+\):[0-9]\+.*/\1 \2/p')
if [[ -n $filepath_and_linenum ]]; then
	open_file
fi

# localpath:linenum:
filepath_and_linenum=$(echo $1 | sed -n 's/^\([^\/][a-zA-Z0-9_\/\.\-]\+\):\([0-9]\+\):.*/\1 \2/p')
if [[ -n $filepath_and_linenum ]]; then
	wd="$(tmux display-message -p -t output '#{pane_current_path}')/"
	filepath_and_linenum=$wd$filepath_and_linenum
	open_file
fi

# python
filepath_and_linenum=$(echo $1 | sed -n 's/^  File "\([a-zA-Z0-9_\/\.\-]\+\.py\)", line \([0-9]\+\), in .*/\1 \2/p')
if [[ -n $filepath_and_linenum ]]; then
	open_file
fi

# no filenames found
echo NO FILENAMES FOUND
```

Then, make sure that script is in your `PATH`, then add these lines to your `tmux.conf` file (the `-d '\n'` part makes sure trailing whitespace is not deleted):
```tmux
set -g mouse on
bind-key -n DoubleClick1Pane select-pane \; copy-mode -M \; send-keys -X select-line \; send-keys -X copy-pipe-and-cancel "xargs -d '\n' open_filepath.sh"
```

Now, when you double-click on a line in tmux, tmux will copy the line and send it to the shell script.
The shell script will then process the line, and if it finds any filename-linenumber pairs, it will execute a Vim command to open the filename at the corresponding line number.

*ref*: https://stackoverflow.com/a/46638561/9357589