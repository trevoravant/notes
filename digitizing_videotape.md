# notes on digitizing Hi8 and VHS tapes

## intro

This writeup describes my experience converting Hi8 and VHS tapes to a digital format using a capture card and Linux.

## capture card

### EasyCAP

There are several capture cards available on the internet, often labelled "EasyCAP". I bought [this one](https://www.amazon.com/gp/product/B01H6OQI1W/ref=ppx_yo_dt_b_asin_title_o00?ie=UTF8&psc=1) from Amazon for $10 and it seemed to work fine. Also, the drivers for this card worked natively in Linux. I used Arch Linux, and didn't need to install any software other than standard Arch Linux packages.

### I-O Data GV-USB2

I also purchased the [GV-USB2-Driver](https://www.amazon.com/dp/B00428BF1Y?ref=ppx_yo2ov_dt_b_product_details&th=1) capture card on the internet. 
Unfortunately, this card did not work natively in Linux. 
To get it to work, I had to use [this repo](https://github.com/Isaac-Lozano/GV-USB2-Driver). 
I followed these steps:
1. git clone the GV-USB2-Driver repo (anywhere you want)
2. in Arch Linux, make sure the `linux-headers` package is installed 
3. `$ make`
4. `# modprobe usbtv`
5. (these instructions are from [here](https://gist.github.com/scaramangado/4e09031d782cbad8a4446ba101f43ef7))
   - run `cat /proc/asound/cards` and note the highest device number
   - run `# insmod gvusb2-audio.ko index=NEW_INDEX` where `NEW_INDEX` is the highest device number plus 1
6. `# insmod gvusb2-video.ko`

## S-Video vs. composite

The capture card has four connectors: audio left (white), audio right (red), composite video (yellow), and S-Video (black). It is worthwhile to note that [S-Video](https://en.wikipedia.org/wiki/S-Video) is known to provide better quality than composite. So if you want to optimize your captures, you may want to consider using S-Video. The image below shows a comparison of captures made with composite and S-Video. S-Video seems to capture more detail, which is especially evident when looking at the detail on the Statue of Liberty's robe.
<!--- code to make the image below:
#!/bin/bash
convert sl_comp.png -font NimbusSans-Regular -pointsize 40 -gravity South \
    -background White -splice 0x60 -annotate +0+3 'composite' sl_comp_note.png
convert sl_svideo.png -font NimbusSans-Regular -pointsize 40 -gravity South \
    -background White -splice 0x60 -annotate +0+3 'S-Video' sl_svid_note.png
montage sl_comp_note.png sl_svid_note.png -tile 2x1 -geometry +0+0 hi8_vhs_digitize_comparison.png
-->

![image info](images/hi8_vhs_digitize_comparison.png)

## video device

The capture card provides both a video and audio stream, which are handled separately in Linux. To deal with the video, we will use standard software from the [Video4Linux](https://linuxtv.org/) project. In the Linux filesystem, the video stream from the capture card will be at a device path in the form `/dev/videoN` where `N` is a number (e.g. 0,1,2...). We can determine this path using the following command: 

``` bash
$ v4l2-ctl --list-devices
```

On my computer, the output of this command tells me that `/dev/video0` corresponds to my capture card (and `/dev/video2` corresponds to my laptop's webcam). Next, the capture card is capable of recording video in several different formats, which can determined with the following command:

``` bash
$ v4l2-ctl --list-formats-ext -d /dev/video0 
```

The output of this command shows several different video formats including 720x480 30 fps (intended to capture  video in the NTSC format), and 720x576 25 fps (intended to capture video in the PAL format). Note that you can use any of these formats to record the video stream, no matter what the format of your Hi8 or VHS tape is. For example, if your video is coming from an NTSC camcorder with an NTSC tape, you can still record in the "PAL format" (i.e. 720x576 25 fps). We will use this information later in our ffmpeg command to to specify which format we would like to caputrue the video at. Note that you can also tell `v4l2` directly to use one of the formats (720x576 25fps in the following example) with the command `$ v4l2-ctl -d /dev/video0 --set-fmt-video=width=720,height=576`.

In my experience, I had American VHS tapes which were probably NTSC, and I found that the video looked better using the capture card's NTSC setting (720x480 30 fps).

## audio device

To deal with the audio from the capture card, we will use software from the [ALSA](https://alsa-project.org) project, which is standard in Linux. In ALSA, the audio device from the capture card will be represented by a device specification in the form `hw:X,Y` where `X` is the card number and `Y` is the device number. Devices can be listed using the following command:

``` bash
$ arecord -l
```

On my computer, the output of this command tells me that the USB capture card corresponds to card 1, and only has device 0. So the capture card audio device is `hw:1,0`.

## capturing the video

### resolution and ratios

First, we note that we captured the video at a resolution of 720x480, which has a width-to-height ratio of 3:2. However, the correct width-to-height ratio to display the video at is actually 4:3. The 3:2 ratio is known as the video's *storage aspect ratio* (SAR), and the 4:3 ratio is known as the video's *display aspect ratio* (DAR). It is acceptable for a digital video to have a SAR that is different from its DAR, in which case the pixels of the video will be rectangular. However, it is often easiest to just convert the video to have square pixels, and make the SAR and DAR the same.

So, if we have a captured a video using a resolution of 720x480 (SAR=3:2), and we want to convert it have square pixels, then the minimum DAR=4:3 resolution without losing quality is 720x540. This is the resolution we will use in the encoding command below.

### method 1: capture the raw video, then encode it

Run the following shell command to record raw video stream from the capture card:

``` bash
ffmpeg -f v4l2 -framerate 30 -video_size 720x480 -thread_queue_size 2048 -i /dev/video0 -f alsa -channel_layout stereo -thread_queue_size 2048 -i hw:1,0 -c:a pcm_s16le -c:v copy -t 02:00:00 file.mkv
```
explanation of options:
* `-framerate 30 -video_size 720x480`: specify the framerate for capturing the video to be 30 FPS, and the resolution for capturing the video to be 720x480, not that these options must correspond to one of the options output from the command we mentioned earlier: `v4l2-ctl --list-formats-ext -d /dev/video0`, also note that these options are only for encoding and not for the final video
* `-thread_queue_size 2048`: this specifies the maximum number of queued packets, for reading both the video and audio streams, if it is too low, then the ffmpeg command won't be able to process the video fast enough and you will probably get a warning
* `-channel_layout stereo`: this was added to remove the warning `Guessed Channel Layout for Input Stream #0.1 : stereo`
* `-c:a pcm_s16le`: this designates the audio codec, which was determined using the command `$ ffprobe -f alsa -i hw:1,0` (I was not able to use `-c:a copy` like I did with `-c:v copy`)
* `-t 02:00:00`: this option sets the recording time to be 2 hours. Change this to whatever length of time you need.

Once capturing is complete, the resulting MKV file will have a size of about 1GB per minute. Next, to convert the captured video using H.265 compression, run
``` bash
ffmpeg -channel_layout stereo -i file.mkv -vf "scale=720x540:flags=lanczos,setsar=1" -c:v libx265 -crf 17 -c:a aac file.mp4
```
explanation of options:
* `-vf "scale=720x540:flags=lanczos,setsar=1"`: `-vf` is an alias for `-filter:v` where the `v` is the `stream_specifier` and stands for "video". There are three parts to this `vf` command: set the resolution of the video, use the Lanczos rescaling algorithm, and set the "sample aspect ratio" (equivalent to PAR (pixel aspect ratio)) to 1[*](https://ffmpeg.org/ffmpeg-filters.html#setdar_002c-setsar).
* `-c:v libx265`: `-c:v` is short for `-codec:v` which tells ffmpeg to set the video encoder. `libx265` is the [x265](https://www.videolan.org/developers/x265.html) encoder which encodes video to the H.265 video compression standard.
* `-crf 17` (optional): CRF means [constant rate factor](https://trac.ffmpeg.org/wiki/Encode/H.264#crf) and lower values generally correspond to better quality but larger file size

Note that the conversion step converts the recorded MKV file to a MP4 file using the H.265 codec, which reduces the video file size by a factor of about 150 (e.g. a 150 GB .avi file became a 1 GB .mp4 file).

### method 2: capture and encode the video simultaneously

Depending on how fast your computer is, and the encoding settings you use, it may be possible to combine the capture and encode steps above into a single command. This will save both time and hard drive space. The following shell command shows how to do this. Note choosing a fast enough `-preset` may be critical to running this command in real-time.

``` bash
ffmpeg -f v4l2 -framerate 30 -video_size 720x480 -thread_queue_size 2048 -i /dev/video0 -f alsa -channel_layout stereo -thread_queue_size 2048 -i hw:1,0 -vf "scale=720x540:flags=lanczos,setsar=1" -c:v libx265 -preset fast -crf 17 -c:a aac -b:a 1G -t 02:00:00 file.mp4
```
explanation of options:
* `-preset fast`: ffmpeg offers [presets](https://trac.ffmpeg.org/wiki/Encode/H.264), which specify how fast the encoding is performed, at the cost of file size. Using a fast enough preset can be critical to performing a simultaneous capture/encoding operation in real-time.

## conversion code for Youtube uploads

If you want to upload this video to Youtube, note that Youtube only supports square pixels (PAR=1). If your video does not have square pixels, then Youtube will convert it to have square pixels after you upload it, so it makes sense to convert your video to square pixels before you upload it. Furthermore, Youtube only allows supports certain video resolutions: 360p, 480p, 720p, etc. The term "480p", for example, describes all resolutions with 480 vertical pixels and progresseive scan (i.e. non-interlaced). Some examples of 480p resolutions are 600x480p, 640x480p, and 800x480p. If you upload a video that is not at one of these resolutions, then Youtube will scale *down* your uploaded video to the nearest supported resolution. For example, for a video with DAR=4:3, Youtube supports the resolutions shown in the first row of the table below. Considering these resolutions, since the original VOB file from the DVD is stored at 720x480, you want to convert your video to at least the 960x720 (720p) resolution to prevent Youtube from downscaling your video.

Furthermore, Youtube will limit the bit rate of uploaded video, and supports higher bit rates for higher resolution videos. For example, if you upload a video at 7Mbps, Youtube may convert it to 1Mbps. So, you can achieve better quality on Youtube by uploading it at a higher resolution. For example, if your original video file is 720x480 with DAR=4:3, then it actually may want to convert and upload it at 1920x1440 (1440p) rather than 960x720 (720p) (and therefore use `scale=1920x1440` in the ffmpeg command). I have found that to obtain a visually lossless Youtube video, you should upload the video to Youtube at at least 1440p.

| DAR | 240p    | 360p    | 480p    | 720p    | 1080p     | 1440p     | 2160p     |
| :-- | :--     | :--     | :--     | :--     | :--       | :--       | :--       |
| 4:3 | 320x240 | 480x360 | 640x480 | 960x720 | 1440x1080 | 1920x1440 | 2880x2160 |

In its processing, Youtube will also convert the audio of an uploaded video. So you want to upload your video to Youtube with the highest quality audio possible so that the resulting audio of the Youtube video will be as close as possible to the original audio. To do this, you can specify an extremely high bit rate (e.g., 1 gigabit) in the `ffmpeg` command to convert the audio. `ffmpeg` won't be able to achieve such a high bit rate, but will use the maximum possible allowable value.

Note that these commands contain several options such as `-pix_fmt`, `-bf`, `-g` which help convert the video to [Youtube's recommended upload encoding settings](https://support.google.com/youtube/answer/1722171?hl=en). It is not clear if all of these settings are necessary, but I use them just for good measure.


``` bash
ffmpeg -f v4l2 -framerate 30 -video_size 720x480 -thread_queue_size 2048 -i /dev/video0 -f alsa -channel_layout stereo -thread_queue_size 2048 -i hw:1,0 -vf "scale=1920x1440:flags=lanczos,setsar=1" -c:v libx264 -preset fast -crf 17 -pix_fmt yuv420p -bf 2 -g 30 -color_primaries 1 -color_trc 1 -colorspace 1 -movflags faststart -c:a flac -t 06:15:00 file.mkv
```

Again note that using a fast enough `-preset` may be critical to running this command in real-time.


## additional note: cutting the video

If you need to cut the end off the video, you can use a command like `ffmpeg -i video.mp4 -ss 00:00:00.0 -to 02:00:00 -c copy video2.mp4`. The `-c copy` option will prevent ffmpeg from re-encoding the video (which would take a very long time).

## additional note: audio noise

There will often be noise in the audio of the captured video. It may be worthwhile to investigate removing this noise using audio editing software such as Audacity. For example, in Audacity you can remove noise with the `Effect > Noise Reduction` tool.

## additional note: GPU-acceleration

The ffmpeg commands in this write-up run on the CPU. However, it is possible to run the ffmpeg command on a GPU, which could potentially be more efficient. For example, with an Nvidia GPU, the `h264_nvenc` encoder can be used in place of `libx264`. However, `h264_nvenc` has different options than `libx264`, so using it requires more investigation.

<!---
NOTES TO SELF:


1) MY STEPS FOR YOUTUBE UPLOAD (WORK IN PROGRESS)

  1a) run the Youtube command above

  1b) cut the video
    
      ffmpeg -i file.mkv -to 05:48:10 -c copy file2.mkv

  1c) extract the audio

      ffmpeg -i file2.mkv -vn -acodec copy file2.flac

  1d) use Audacity to remove the noise from the audio, and save the processed audio as a flac file

  1e) convert the processed audio to AAC format

      ffmpeg -i file.flac -c:a aac -b:a 1G file.aac

  1f) combine the video stream and AAC audio into one file

      ffmpeg -i file2.mkv -i file2new.aac -map 0:v -map 1:a -c copy file3.mp4
-->
